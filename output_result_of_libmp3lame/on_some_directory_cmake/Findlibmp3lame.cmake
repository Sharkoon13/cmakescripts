# Find the libmp3lame
#
# available types:
# bin files     #search each entry in binary files and create output vars libmp3lame_ path , also create libmp3lame_BIN_DIRS directories for PATH
# include files #search one of list of header files and save path to it, generate two variables libmp3lame_INCLUDE_DIR and libmp3lame_INCLUDE_DIRS
# library files #search each entry in library files and create output vars libmp3lame_LIBNAME@ path , all variables together in libmp3lame_LIBRARIES and also generate libmp3lame_LIBRARY_DIRS for LD_LIBRARY_PATH
#
# next variables:
#
# libmp3lame_FOUND            -  # set to 1 if module( libmp3lame ) found , 0 otherwise
# libmp3lame_INCLUDE_DIR      -  # if project has one headers dir , include it.    | if all headers in include_files
# libmp3lame_INCLUDE_DIRS     -  # contains all project include dirs               |
# libmp3lame_ -  # for each executable file in list ( bin_files ) generate path to exec file
# libmp3lame_BINARIES         -  # all executables in list
# libmp3lame_BIN_DIR          -  # first directory in list BIN_DIRS ( if one executable )
# libmp3lame_BIN_DIRS         -  # all directories with executables without duplicates
# libmp3lame_LIBNAME@         -  #
# libmp3lame_LIBRARY          -  # the '-I' preprocessor flags (w/o the '-I') headers location
# libmp3lame_LIBRARIES        -  # all libraries with full paths
# libmp3lame_LIBRARY_DIRS     -  # all libraries directories
#
# Hints
# set ``libmp3lame_ROOTDIR`` variable path to libmp3lame
# or you can set
# set ``libmp3lame_ROOTDIR`` variable path to libmp3lame

#clear all
unset(bins)
unset(bins CACHE )
unset(headers)
unset(headers CACHE )
unset(libs)
unset(libs CACHE )
unset(pkg_config_files)
unset(pkg_config_files CACHE )
unset(pkg_names)
unset(pkg_names CACHE )

unset(def_path )
unset(def_path CACHE )

unset(_bin_)
unset(_bin_ CACHE )
unset(_header_path_)
unset(_header_path_ CACHE )
unset(_library_)
unset(_library_ CACHE )

#begin

###DEBUG
##MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] bins ${bins}                        ")
##MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] headers ${headers}                  ")
##MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] libs ${libs}                        ")
##MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] pkg_config_files ${pkg_config_files}")
##MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] pkg_names ${pkg_names}              ")
##MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] _bin_ ${_bin_}                      ")
##MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] _header_path_ ${_header_path_}      ")
##MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] _library_ ${_library_}              ")
###DEBUG

LIST(APPEND bins lame)
LIST(APPEND headers lame/lame.h)
LIST(APPEND libs mp3lame)

LIST(APPEND pkg_config_files mp3lame.pc)  # files in pkg_config/ file1.pc file2.pc file3.pc
LIST(APPEND pkg_names mp3lame)         # project in pkg_config/*.pc file: project  projectNum

##DEBUG
#MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] bins             ${bins}                     ")
#MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] headers          ${headers}                  ")
#MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] libs             ${libs}                     ")
#MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] pkg_config_files ${pkg_config_files}                        ")
#MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] pkg_names        ${pkg_names}                        ")
##DEBUG

if (SmartSdk_DEBUG)
    message(STATUS "--------------------------Findlibmp3lame--------------------------" )
endif (SmartSdk_DEBUG)

if ( pkg_config_files )

    foreach(pcf ${pkg_config_files} )

        ##DEBUG
        #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] pcf              ${pcf}                     ")
        #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] pkg_config_files ${pkg_config_files}        ")
        ##DEBUG

        find_path(  libmp3lame_PKG_DIR
                    NAMES 
                    PATHS ${libmp3lame_ROOTDIR} ${libmp3lame_ROOTDIR} 
                    PATH_SUFFIXES
                    lib/pkgconfig
                    lib/i386-linux-gnu/pkgconfig
                    lib/x86_64-linux-gnu/pkgconfig
                    lib64/pkgconfig
                    lib64/i386-linux-gnu/pkgconfig
                    lib64/x86_64-linux-gnu/pkgconfig
                    share/pkgconfig
                    pkgconfig
                    
                    )

        ##DEBUG
        #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] libmp3lame_PKG_DIR ${libmp3lame_PKG_DIR}        ")
        ##DEBUG


        if (NOT libmp3lame_PKG_DIR STREQUAL libmp3lame_PKG_DIR-NOTFOUND )

            if (SmartSdk_DEBUG)
                message(STATUS "found libmp3lame_PKG_DIR  = ${libmp3lame_PKG_DIR}" )
            endif (SmartSdk_DEBUG)

            #    include pkgconfig and get all variables
            set( ENV{PKG_CONFIG_PATH} ${libmp3lame_PKG_DIR}:$ENV{PKG_CONFIG_PATH} )

            ##DEBUG
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] ENV{PKG_CONFIG_PATH} $ENV{PKG_CONFIG_PATH}")
            ##DEBUG
        endif (NOT libmp3lame_PKG_DIR STREQUAL libmp3lame_PKG_DIR-NOTFOUND )

    endforeach()

endif ( pkg_config_files )

if ( pkg_names )

    find_package(PkgConfig REQUIRED QUIET )

    foreach( pkg ${pkg_names})

        ##DEBUG
        #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] pkg ${pkg}        ")
        #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] pkg_names ${pkg_names}        ")
        ##DEBUG

        string(TOUPPER ${pkg} OUT_PKG_NAME)  #output names , z.B. openssl - OPENSSL , zlib - ZLIB


        ##DEBUG
        #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] OUT_PKG_NAME ${OUT_PKG_NAME}        ")
        ##DEBUG

        pkg_check_modules (${OUT_PKG_NAME} REQUIRED ${pkg})

        if ( ${OUT_PKG_NAME}_FOUND )

            foreach( lib ${${OUT_PKG_NAME}_LIBRARIES} )  #foreach library found full path in library dirs

                ##DEBUG
                #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] lib ${lib}        ")
                #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] ${OUT_PKG_NAME}_LIBRARIES ${${OUT_PKG_NAME}_LIBRARIES}        ")
                ##DEBUG

                find_library ( _library_
                        NAMES ${lib}
                        HINTS ${${OUT_PKG_NAME}_LIBRARY_DIRS}
                        PATH_SUFFIXES
                        lib
                        lib64
                        lib/x86_64-linux-gnu
                        lib64/x86_64-linux-gnu
                        lib/i386-linux-gnu
                        lib64/i386-linux-gnu
                        
                        )

                ##DEBUG
                #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] _library_ ${_library_}        ")
                ##DEBUG

                if (SmartSdk_DEBUG)
                    message(STATUS "found libmp3lame_LIBRARY  = ${_library_}" )
                endif (SmartSdk_DEBUG)

                LIST(APPEND LIBRARIES ${_library_} ) #and append to list of libraries

                ##DEBUG
                #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] LIBRARIES ${LIBRARIES}        ")
                ##DEBUG

            endforeach()


            ##DEBUG
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] LIBRARIES ${LIBRARIES}        ")
            ##DEBUG

            list(REMOVE_DUPLICATES LIBRARIES )


            ##DEBUG
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] LIBRARIES ${LIBRARIES}        ")
            ##DEBUG

            ##DEBUG
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] libmp3lame_INCLUDE_DIRS  ${libmp3lame_INCLUDE_DIRS}        ")
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] libmp3lame_LDFLAGS       ${libmp3lame_LDFLAGS}        ")
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] libmp3lame_LDFLAGS_OTHER ${libmp3lame_LDFLAGS_OTHER}        ")
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] libmp3lame_CFLAGS        ${libmp3lame_CFLAGS}        ")
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] libmp3lame_CFLAGS_OTHER  ${libmp3lame_CFLAGS_OTHER}        ")
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] libmp3lame_LIBRARIES     ${libmp3lame_LIBRARIES}        ")
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] libmp3lame_LIBRARY_DIRS  ${libmp3lame_LIBRARY_DIRS}        ")
            ##DEBUG

            list(APPEND libmp3lame_INCLUDE_DIRS  ${${OUT_PKG_NAME}_INCLUDE_DIRS}  )
            list(GET ${OUT_PKG_NAME}_INCLUDE_DIRS 0 libmp3lame_INCLUDE_DIR)
            list(APPEND libmp3lame_LDFLAGS       ${${OUT_PKG_NAME}_LDFLAGS}       )
            list(APPEND libmp3lame_LDFLAGS_OTHER ${${OUT_PKG_NAME}_LDFLAGS_OTHER} )
            list(APPEND libmp3lame_CFLAGS        ${${OUT_PKG_NAME}_CFLAGS}        )
            list(APPEND libmp3lame_CFLAGS_OTHER  ${${OUT_PKG_NAME}_CFLAGS_OTHER}  )
            list(APPEND libmp3lame_LIBRARIES     ${LIBRARIES} )
            list(APPEND libmp3lame_LIBRARY_DIRS  ${${OUT_PKG_NAME}_LIBRARY_DIRS} )


            ##DEBUG
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] libmp3lame_INCLUDE_DIRS  ${libmp3lame_INCLUDE_DIRS}        ")
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] libmp3lame_LDFLAGS       ${libmp3lame_LDFLAGS}        ")
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] libmp3lame_LDFLAGS_OTHER ${libmp3lame_LDFLAGS_OTHER}        ")
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] libmp3lame_CFLAGS        ${libmp3lame_CFLAGS}        ")
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] libmp3lame_CFLAGS_OTHER  ${libmp3lame_CFLAGS_OTHER}        ")
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] libmp3lame_LIBRARIES     ${libmp3lame_LIBRARIES}        ")
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] libmp3lame_LIBRARY_DIRS  ${libmp3lame_LIBRARY_DIRS}        ")
            ##DEBUG

        endif( ${OUT_PKG_NAME}_FOUND )

    endforeach()

else ( pkg_names )

    #
    #  find headers and include dirs
    #

    if ( headers )
        foreach(header ${headers} )

            ##DEBUG
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] header ${header}        ")
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] headers ${headers}        ")
            ##DEBUG
                
            find_path (_header_path_
                    NAMES ${header}
                    PATHS ${libmp3lame_ROOTDIR} ${libmp3lame_ROOTDIR}
                    PATH_SUFFIXES header header64 . 
                    )


            ##DEBUG
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] _header_path_ ${_header_path_}        ")
            ##DEBUG
                
            if (SmartSdk_DEBUG)
                message(STATUS "found libmp3lame_INCLUDE_DIR  = ${_header_path_}" )
            endif (SmartSdk_DEBUG)


            ##DEBUG
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] libmp3lame_INCLUDE_DIRS ${libmp3lame_INCLUDE_DIRS}        ")
            ##DEBUG
                
            LIST(APPEND libmp3lame_INCLUDE_DIRS ${_header_path_})

            ##DEBUG
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] libmp3lame_INCLUDE_DIRS ${libmp3lame_INCLUDE_DIRS}        ")
            ##DEBUG

        endforeach()


        ##DEBUG
        #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] libmp3lame_INCLUDE_DIRS ${libmp3lame_INCLUDE_DIRS}        ")
        ##DEBUG

        list(REMOVE_DUPLICATES libmp3lame_INCLUDE_DIRS)

        ##DEBUG
        #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] libmp3lame_INCLUDE_DIRS ${libmp3lame_INCLUDE_DIRS}        ")
        ##DEBUG

        list(GET libmp3lame_INCLUDE_DIRS 0 libmp3lame_INCLUDE_DIR)

    endif( headers )


    #
    #  find libraries and library dirs
    #

    if ( libs )
        foreach(library ${libs} )


            ##DEBUG
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] library ${library}        ")
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] libs ${libs}        ")
            ##DEBUG
            
            find_library ( _library_
                    NAMES ${library}
                    HINTS ${libmp3lame_ROOTDIR} ${libmp3lame_ROOTDIR}
                    PATH_SUFFIXES
                    lib
                    lib64
                    lib/x86_64-linux-gnu
                    lib64/x86_64-linux-gnu
                    lib/i386-linux-gnu
                    lib64/i386-linux-gnu
                    
                    )

            ##DEBUG
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] _library_ ${_library_}        ")
            ##DEBUG

            get_filename_component(_library_name_ ${_library_} NAME_WE)
            get_filename_component(_library_dir_  ${_library_} DIRECTORY)

            string(TOUPPER ${_library_name_} _library_name_)

            set( libmp3lame_${_library_name_} ${_library_} )

            if (SmartSdk_DEBUG)
                message(STATUS "found libmp3lame_LIBRARY  = ${_library_}" )
            endif (SmartSdk_DEBUG)


            ##DEBUG
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] libmp3lame_LIBRARIES ${libmp3lame_LIBRARIES}        ")
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] libmp3lame_LIBRARY_DIRS ${libmp3lame_LIBRARY_DIRS}        ")
            ##DEBUG


            LIST(APPEND libmp3lame_LIBRARIES ${_library_} )
            LIST(APPEND libmp3lame_LIBRARY_DIRS ${_library_dir_} )

            ##DEBUG
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] libmp3lame_LIBRARIES ${libmp3lame_LIBRARIES}        ")
            #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] libmp3lame_LIBRARY_DIRS ${libmp3lame_LIBRARY_DIRS}        ")
            ##DEBUG

        endforeach()

        list(REMOVE_DUPLICATES libmp3lame_LIBRARY_DIRS)

        ##DEBUG
        #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] libmp3lame_LIBRARY_DIRS ${libmp3lame_LIBRARY_DIRS}        ")
        ##DEBUG


    endif( libs )

endif ( pkg_names )


#
#  find binaries
#

if ( bins )
    foreach(bin ${bins} )

        ##DEBUG
        #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] bin ${bin}        ")
        #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] bins ${bins}        ")
        ##DEBUG


        find_program (  _bin_
                NAMES ${bin}
                PATHS ${libmp3lame_ROOTDIR} ${libmp3lame_ROOTDIR}
                PATH_SUFFIXES bin bin64 .  )

        ##DEBUG
        #MESSAGE(STATUS "[DEBUG][${CMAKE_CURRENT_LIST_LINE}] _bin_ ${_bin_}        ")
        ##DEBUG

        get_filename_component(_bin_name_ ${_bin_} NAME_WE)
        get_filename_component(_bin_dir_  ${_bin_} DIRECTORY)

        string(TOUPPER ${_bin_name_} _bin_name_)

        set( libmp3lame_${_bin_name_} ${_bin_} )
        set( libmp3lame_BIN  ${_bin_} )

        if (SmartSdk_DEBUG)
            message(STATUS "found libmp3lame_BIN_DIR  = ${_bin_dir_}" )
        endif (SmartSdk_DEBUG)

        LIST(APPEND libmp3lame_BINARIES ${_bin_} )
        LIST(APPEND libmp3lame_BIN_DIRS ${_bin_dir_})

    endforeach()

    list(REMOVE_DUPLICATES libmp3lame_BIN_DIRS)
    list(REMOVE_DUPLICATES libmp3lame_BINARIES)
    list(GET libmp3lame_BIN_DIRS 0 libmp3lame_BIN_DIR)
    set(libmp3lame_PATH ${libmp3lame_BIN_DIRS})

endif ( bins )


# Support the REQUIRED and QUIET arguments, and set libmp3lame_FOUND if found.
include (FindPackageHandleStandardArgs)

unset( ARGS )

if ( bins)
    set( ARGS libmp3lame_BINARIES )
endif ( bins)

if ( headers OR pkg_names )
    set( ARGS libmp3lame_INCLUDE_DIRS ${ARGS} )
endif ( headers OR pkg_names )

if ( libs OR pkg_names )
    set( ARGS libmp3lame_LIBRARIES ${ARGS} )
endif ( libs OR pkg_names )

FIND_PACKAGE_HANDLE_STANDARD_ARGS (libmp3lame DEFAULT_MSG ${ARGS} )


if (libmp3lame_FOUND)
    if (SmartSdk_DEBUG)
        if ( bins )
            message (STATUS "libmp3lame bin = ${libmp3lame_BINARIES}")
            message (STATUS "libmp3lame libmp3lame_BIN_DIRS = ${libmp3lame_BIN_DIRS}")
        endif ( bins )
        if ( headers  OR pkg_names )
            message (STATUS "libmp3lame INCLUDE_DIR = ${libmp3lame_INCLUDE_DIR}")
            message (STATUS "libmp3lame INCLUDE_DIRS = ${libmp3lame_INCLUDE_DIRS}")
        endif ( headers  OR pkg_names )
        if ( libs  OR pkg_names )
            message (STATUS "libmp3lame libraries = ${libmp3lame_LIBRARIES}")
            message (STATUS "libmp3lame library directories = ${libmp3lame_LIBRARY_DIRS}")
        endif ( libs  OR pkg_names )
    endif (SmartSdk_DEBUG)
else ()
    message (WARNING "No libmp3lame found")
endif()

unset( ARGS )

if ( bins)
    set( ARGS libmp3lame_BINARIES )
endif ( bins)

if ( headers OR pkg_names )
    set( ARGS libmp3lame_INCLUDE_DIRS ${ARGS} )
endif ( headers OR pkg_names )

if ( libs OR pkg_names )
    set( ARGS libmp3lame_LIBRARIES  ${ARGS} )
endif ( libs OR pkg_names )

mark_as_advanced ( ${ARGS} )


if (SmartSdk_DEBUG)
    unset(_variableNames)
    unset(_variableNames CACHE )


    get_cmake_property(_variableNames VARIABLES)
    message(STATUS "Available variables also:")
    foreach (_variableName ${_variableNames})
        string(FIND ${_variableName} libmp3lame idx )
        if ( ${idx} EQUAL 0 )
            message(STATUS "${_variableName} = ${${_variableName}}")
        endif( ${idx} EQUAL 0 )
    endforeach()

endif (SmartSdk_DEBUG)




if (SmartSdk_DEBUG)
    message(STATUS "--------------------------Findlibmp3lame--------------------------" )
endif (SmartSdk_DEBUG)
