



#unset types for each extra variable
unset( MP3LAME_ROOT ) #scoped variable 
unset( MP3LAME_ROOT CACHE) #cached variable 
unset( ENV{MP3LAME_ROOT} ) #enviroment variable 

#restore original CMAKE_PREFIX_PATH 
if( CMAKE_PREFIX_PATH_ORIG ) 
    set( CMAKE_PREFIX_PATH ${CMAKE_PREFIX_PATH_ORIG}) 
endif( CMAKE_PREFIX_PATH_ORIG ) 

#restore original ENV{PATH}
if( PATH_ORIG ) 
    set( ENV{PATH} ${PATH_ORIG}) 
endif( PATH_ORIG ) 

#restore original ENV{PKG_CONFIG_PATH}
if( PKG_CONFIG_PATH_ORIG ) 
    set( ENV{PKG_CONFIG_PATH} ${PKG_CONFIG_PATH_ORIG}) 
endif( PKG_CONFIG_PATH_ORIG ) 



#set scoped variables
set( MP3LAME_ROOT ${SMARTSDK_ROOT}/libmp3lame/master) 
set( libmp3lame_ROOTDIR ${SMARTSDK_ROOT}/libmp3lame/master) 
set( libmp3lame_PKG_DIR ${SMARTSDK_ROOT}/libmp3lame/master/lib/pkgconfig) 
set( libmp3lame_PATH ${SMARTSDK_ROOT}/libmp3lame/master/bin) 
set( libmp3lame_LDPATH ${SMARTSDK_ROOT}/libmp3lame/master/lib) 
