function(CompileICE)
    set(FLAGS      #Flags for generating ice slices.
        C++11      #Generate sample implementations for C++11 mapping.
        C++98      #Generate sample implementations for C++98 mapping.
        INCLUDE    #auto add instruction |  include_directories("${GENDIR}")   |
        ICE        #Allow reserved Ice prefix in Slice identifiers. deprecated: use instead [["ice-prefix"]] metadata.
        UNDERSCORE #Allow underscores in Slice identifiers. deprecated: use instead [["underscore"]] metadata.
        CHECKSUM   #Generate checksums for Slice definitions.
        DEBUG      #Print debug messages in this script.
        DEBUG_ICE  #Print debug messages on slice2${LANG}.
        E          #-E                       Print preprocessor output on stdout.
        VALIDATE   #--validate               Validate command line options.
        DEPEND_XML #--depend-xml             Generate dependencies in XML format.
        DEPEND     #--depend                 Generate Makefile dependencies.
        ON_CONFIG  #set flag to generate ice files at config time. Default false (generate at build time)
        CLEAN      #auto add dependencies (gen target depend clean target)
        )

    set(PARAMS
        ICE_FILES_ROOT #Path to root of Ice interfaces (/some/path/to/file.ice and ICE_FILES "to/file.ice" -> ICE_FILES_ROOT = /some/path/)
        DLL_EXPORT     #DLL_EXPORT SYMBOL : Use SYMBOL for DLL exports.deprecated: use instead [["cpp:dll-export:SYMBOL"]] metadata.
        SOURCE_EXT     #--source-ext EXT         Use EXT instead of the default `cpp' extension.
        HEADER_EXT     #--header-ext EXT         Use EXT instead of the default `h' extension.
        DEPEND_FILE    #--depend-file FILE       Write dependencies to FILE instead of standard output.
        OUTPUT_DIR     #--output-dir DIR         Create files in the directory DIR.
        OUTPUT_HDR_VAR #name of variable , which will be contain a list of generated header files
        OUTPUT_CPP_VAR #name of variable , which will be contain a list of generated source files
        COMPILER_NAME  #on of possible names for slice2* (CPP , CS | PHP | JAVA | PY | JS | RB);
        TARGET_NAME    #name of generated target (for next add_dependencies). Default "GenerateIce${COMPILER_NAME}"
        CLEAN_TARGET_NAME    #name of generated target (for next add_dependencies). Default "CleanIce${COMPILER_NAME}"
        )
    set(OPTIONS
#        ADDITIONAL_INCLUDE_DIRS  #--include-dir DIR  Use DIR as the header include directory in source files.
        ADDITIONAL_HEADERS_GUARD #--add-header HDR[,GUARD] Add #include for HDR (with guard GUARD) to generated source file.
        ICE_INCLUDE_DIRS         #-IDIR      Put DIR in the include file search path.
        DNAME                    #-DNAME     Define NAME as 1.
        UNAME                    #-UNAME     Remove any definition for NAME.
        DNAME_DEF                #-DNAME=DEF Define NAME as DEF.
        ICE_FILES                #slice files list with relative path. (${ICE_FILES_ROOT}/add/path/file.ice)
        FLAGS                    #compile flags for source groups of icegenerate
        )

    cmake_parse_arguments(ARG
            "${FLAGS}"
            "${PARAMS}"
            "${OPTIONS}"
            ${ARGN}
    )

    #which slice2 we want to call (cpp , cs, js , java)
    if (ARG_COMPILER_NAME)
        set(LANG ${ARG_COMPILER_NAME})
    else (ARG_COMPILER_NAME)
        set(LANG "CPP")
        message(STATUS "Set default Slice compiler -> CPP ")
    endif (ARG_COMPILER_NAME)

    if (ARG_TARGET_NAME)
        set(TARGET_NAME ${ARG_TARGET_NAME})
    else (ARG_TARGET_NAME)
        set(TARGET_NAME GenerateIce${LANG})
    endif (ARG_TARGET_NAME)

    if (ARG_CLEAN_TARGET_NAME)
        set(CLEAN_TARGET_NAME ${ARG_CLEAN_TARGET_NAME})
    else (ARG_CLEAN_TARGET_NAME)
        set(CLEAN_TARGET_NAME CleanIce${LANG})
    endif (ARG_CLEAN_TARGET_NAME)

    #check main variables:
    #output headers files
    if (ARG_OUTPUT_HDR_VAR)
        set(OUTPUT_HDR_VAR ${ARG_OUTPUT_HDR_VAR})
    else (ARG_OUTPUT_HDR_VAR)
        set(OUTPUT_HDR_VAR GEN_ICE_HDRS)
        message(STATUS "List of generated header files contains a variable GEN_ICE_HDRS")
    endif (ARG_OUTPUT_HDR_VAR)


    #output sources files
    if (ARG_OUTPUT_CPP_VAR)
        set(OUTPUT_CPP_VAR ${ARG_OUTPUT_CPP_VAR})
    else (ARG_OUTPUT_CPP_VAR)
        set(OUTPUT_CPP_VAR GEN_ICE_SRCS)
        message(STATUS "List of generated source files contains a variable GEN_ICE_SRCS")
    endif (ARG_OUTPUT_CPP_VAR)

    #Checking all Flags;
    unset(SLICE_FLAGS_VAR)

    if (LANG STREQUAL "CPP")
        if (ARG_C++98)
            set(SLICE_FLAGS_VAR --impl-c++98)
        endif (ARG_C++98)

        if (ARG_C++11)
            set(SLICE_FLAGS_VAR --impl-c++11)
        endif (ARG_C++11)
    endif (LANG STREQUAL "CPP")

    if (ARG_ICE)
        set(SLICE_FLAGS_VAR ${SLICE_FLAGS_VAR} --ice)
    endif (ARG_ICE)

    if (ARG_UNDERSCORE)
        set(SLICE_FLAGS_VAR ${SLICE_FLAGS_VAR} --underscore)
    endif (ARG_UNDERSCORE)

    if (ARG_CHECKSUM)
        set(SLICE_FLAGS_VAR ${SLICE_FLAGS_VAR} --checksum)
    endif (ARG_CHECKSUM)

    if (ARG_VALIDATE)
        set(SLICE_FLAGS_VAR ${SLICE_FLAGS_VAR} --validate)
    endif (ARG_VALIDATE)

    if (ARG_DEBUG_ICE)
        set(SLICE_FLAGS_VAR ${SLICE_FLAGS_VAR} --debug)
    endif (ARG_DEBUG_ICE)

    if (ARG_E)
        set(SLICE_FLAGS_VAR ${SLICE_FLAGS_VAR} -E)
    endif (ARG_E)

    if (ARG_DEPEND)
        set(SLICE_FLAGS_VAR ${SLICE_FLAGS_VAR} --depend)
    endif (ARG_DEPEND)

    if (ARG_DEPEND_XML)
        set(SLICE_FLAGS_VAR ${SLICE_FLAGS_VAR} --depend-xml)
    endif (ARG_DEPEND_XML)
    #Checking all Flags;

    unset(SLICE_PARAMS_VAR)
    unset(SLICE_INTERFACES_ROOTDIR)

    #Checking parameters
    if (ARG_DLL_EXPORT)
        set(SLICE_PARAMS_VAR ${ARG_DLL_EXPORT})
    endif (ARG_DLL_EXPORT)

    if (ARG_ICE_FILES_ROOT)
        set(SLICE_INTERFACES_ROOTDIR ${ARG_ICE_FILES_ROOT})
    else (ARG_ICE_FILES_ROOT)
        set(SLICE_INTERFACES_ROOTDIR ${CMAKE_CURRENT_LIST_DIR}/../ice)
        message(STATUS "Not set ICE_FILES_ROOT , search all in \"${CMAKE_CURRENT_LIST_DIR}/..\" ")
    endif (ARG_ICE_FILES_ROOT)

    if (LANG STREQUAL "CPP")
        if (ARG_SOURCE_EXT)
            set(SLICE_PARAMS_VAR ${SLICE_PARAMS_VAR} --source-ext ${ARG_SOURCE_EXT})
        endif (ARG_SOURCE_EXT)

        if (ARG_HEADER_EXT)
            set(SLICE_PARAMS_VAR ${SLICE_PARAMS_VAR} --header-ext ${ARG_HEADER_EXT})
        endif (ARG_HEADER_EXT)

    endif (LANG STREQUAL "CPP")


    if (ARG_DEPEND_FILE)
        set(SLICE_PARAMS_VAR ${SLICE_PARAMS_VAR} --depend-file ${ARG_DEPEND_FILE})
    endif (ARG_DEPEND_FILE)

    if (ARG_OUTPUT_DIR)
        set(SLICE_OUTPUT_DIR_BASE ${ARG_OUTPUT_DIR})
    else (ARG_OUTPUT_DIR)
        set(SLICE_OUTPUT_DIR_BASE ${CMAKE_BINARY_DIR}/ice_gen)
        message(STATUS "Not set OUTPUT_DIR , generate files to \"${CMAKE_BINARY_DIR}/ice_gen\" ")
    endif (ARG_OUTPUT_DIR)

    if (NOT EXISTS ${SLICE_OUTPUT_DIR_BASE})
        file(MAKE_DIRECTORY ${SLICE_OUTPUT_DIR_BASE})
    endif (NOT EXISTS ${SLICE_OUTPUT_DIR_BASE})

    #Checking parameters

    unset(SLICE_OPTIONS_VAR)

    if (LANG STREQUAL "CPP")
        if (ARG_FLAGS)
            set(COMPILE_FLAGS ${ARG_FLAGS})
        else (ARG_FLAGS)
            set(COMPILE_FLAGS -Wno-switch-default -Wno-redundant-decls -Wno-overloaded-virtual -Wno-old-style-cast -Wno-deprecated-declarations)
        endif (ARG_FLAGS)

        message(STATUS "Apply next flags as compile flags: ${COMPILE_FLAGS}")

    endif (LANG STREQUAL "CPP")

    #Checking options
    if (ARG_DNAME)
        foreach (_name ${ARG_DNAME})
            set(SLICE_OPTIONS_VAR ${SLICE_OPTIONS_VAR} -D${_name})
        endforeach()
    endif (ARG_DNAME)

    if (ARG_UNAME)
        foreach (_name ${ARG_UNAME})
            set(SLICE_OPTIONS_VAR ${SLICE_OPTIONS_VAR} -U${_name})
        endforeach()
    endif (ARG_UNAME)

    if (ARG_DNAME_DEF)
        foreach (_name_def ${ARG_DNAME_DEF})
            set(SLICE_OPTIONS_VAR ${SLICE_OPTIONS_VAR} -D${_name_def})
        endforeach()
    endif (ARG_DNAME_DEF)

    if (ARG_ICE_INCLUDE_DIRS)
        foreach (_inc_dir ${ARG_ICE_INCLUDE_DIRS})
            set(SLICE_OPTIONS_VAR ${SLICE_OPTIONS_VAR} -I${_inc_dir})
        endforeach()
    endif (ARG_ICE_INCLUDE_DIRS)

    set(SLICE_OPTIONS_VAR ${SLICE_OPTIONS_VAR} -I${Ice_SLICE_DIR})

#    if (ARG_ADDITIONAL_INCLUDE_DIRS)
#        foreach (_inc_dir ${ARG_ADDITIONAL_INCLUDE_DIRS})
#            set(SLICE_OPTIONS_VAR ${SLICE_OPTIONS_VAR} --include-dir ${_inc_dir})
#        endforeach()
#    endif (ARG_ADDITIONAL_INCLUDE_DIRS)


    if (LANG STREQUAL "CPP")
        if (ARG_ADDITIONAL_HEADERS_GUARD)
            foreach (_hdr_grd ${ARG_ADDITIONAL_HEADERS_GUARD})
                set(SLICE_OPTIONS_VAR ${SLICE_OPTIONS_VAR} --add-header ${_hdr_grd})
            endforeach()
        endif (ARG_ADDITIONAL_HEADERS_GUARD)

    endif (LANG STREQUAL "CPP")

     # set target extensions
    if (LANG STREQUAL "CPP")
        list(APPEND GEN_FILE_EXT cpp)
        list(APPEND GEN_FILE_EXT h)
    endif (LANG STREQUAL "CPP")

    if (LANG STREQUAL "JS")
        list(APPEND GEN_FILE_EXT js)
    endif (LANG STREQUAL "JS")

    if (LANG STREQUAL "JAVA")
        list(APPEND GEN_FILE_EXT java)
    endif (LANG STREQUAL "JAVA")

    #if flag CLEAN set , cleaning up...
    if (ARG_CLEAN AND ARG_ON_CONFIG)
		set(CLEAN_DIR "${SLICE_OUTPUT_DIR_BASE}")
		set(CLEAN_EXT ${GEN_FILE_EXT})
		include(${SmartSdk_CMAKE_DIR}/CompileClean.cmake)
    endif ()

    #if flag include is defined , add include_directories instruction.
    if (ARG_INCLUDE)
        include_directories(${SLICE_OUTPUT_DIR_BASE})
    endif (ARG_INCLUDE)

    if (NOT ARG_ICE_FILES)
        message(STATUS "Not set ICE_FILES , using all files from \"${SLICE_INTERFACES_ROOTDIR}\" ")
        file (GLOB_RECURSE ARG_ICE_FILES
                RELATIVE ${SLICE_INTERFACES_ROOTDIR}
                "${SLICE_INTERFACES_ROOTDIR}/*.ice"
                )
    endif (NOT ARG_ICE_FILES)

    if (NOT ARG_ON_CONFIG)
        unset(TARGET_CMD_LIST)
        unset(CLEAN_TARGET_CMD_LIST)
    endif (NOT ARG_ON_CONFIG)

    #Collect all slice files on ICE_FILES_ROOT/*.ice
    foreach(slice ${ARG_ICE_FILES})

        set(depend_files ${SLICE_INTERFACES_ROOTDIR}/${slice})

        if (LANG STREQUAL "CPP")
            string(REPLACE ".ice" ".h" ice_hdr ${slice})
            string(REPLACE ".ice" ".cpp" ice_src ${slice})
        endif (LANG STREQUAL "CPP")

        if (LANG STREQUAL "JS")
            string(REPLACE ".ice" ".js" ice_src ${slice})
        endif (LANG STREQUAL "JS")

        if (LANG STREQUAL "JAVA")
            string(REPLACE ".ice" ".java" ice_src ${slice})
        endif (LANG STREQUAL "JAVA")

        get_filename_component(filepath ${ice_src} PATH)

        set(OUTPUT_PATH ${SLICE_OUTPUT_DIR_BASE}/${filepath})

        if (LANG STREQUAL "CPP")
            get_filename_component(name_h ${ice_hdr} NAME)
            get_filename_component(name_c ${ice_src} NAME)

                if (filepath)
                    list(APPEND ${OUTPUT_HDR_VAR} ${SLICE_OUTPUT_DIR_BASE}/${filepath}/${name_h})
                    list(APPEND ${OUTPUT_CPP_VAR} ${SLICE_OUTPUT_DIR_BASE}/${filepath}/${name_c})
                else (filepath)
                    list(APPEND ${OUTPUT_HDR_VAR} ${SLICE_OUTPUT_DIR_BASE}/${name_h})
                    list(APPEND ${OUTPUT_CPP_VAR} ${SLICE_OUTPUT_DIR_BASE}/${name_c})
                endif (filepath)

        endif (LANG STREQUAL "CPP")

        if (LANG STREQUAL "JS")

            get_filename_component(name_js ${ice_src} NAME)
            if (filepath)
                list(APPEND ${OUTPUT_CPP_VAR} ${SLICE_OUTPUT_DIR_BASE}/${filepath}/${name_js})
            else (filepath)
                list(APPEND ${OUTPUT_CPP_VAR} ${SLICE_OUTPUT_DIR_BASE}/${name_js})
            endif (filepath)

        endif (LANG STREQUAL "JS")

        if (LANG STREQUAL "JAVA")

            get_filename_component(name_java ${ice_src} NAME)
            if (filepath)
                list(APPEND ${OUTPUT_CPP_VAR} ${SLICE_OUTPUT_DIR_BASE}/${filepath}/${name_java})
            else (filepath)
                list(APPEND ${OUTPUT_CPP_VAR} ${SLICE_OUTPUT_DIR_BASE}/${name_java})
            endif (filepath)

        endif (LANG STREQUAL "JAVA")

        set(SLICE_PARAMS_VAR_OUT --output-dir ${OUTPUT_PATH})

        if (LANG STREQUAL "CPP")
            set(SLICE_PARAMS_VAR_OUT_INC ${SLICE_OPTIONS_VAR} --include-dir ${OUTPUT_PATH})
        endif (LANG STREQUAL "CPP")


        #generate files.

        if (ARG_ON_CONFIG)

            if (ARG_DEBUG)
                message(STATUS "______________________________________________________")
                message(STATUS "execute_process(COMMAND ${Ice_SLICE2${LANG}_EXECUTABLE} ")
                message(STATUS "        ${SLICE_FLAGS_VAR} ")
                message(STATUS "        ${SLICE_PARAMS_VAR} ")
                message(STATUS "        ${SLICE_OPTIONS_VAR} ")
                message(STATUS "        ${SLICE_PARAMS_VAR_OUT} ")
                message(STATUS "        ${SLICE_PARAMS_VAR_OUT_INC} ")
                message(STATUS "        ${slice} ")
                message(STATUS "        WORKING_DIRECTORY ${SLICE_INTERFACES_ROOTDIR} ")
                message(STATUS "______________________________________________________")
            endif (ARG_DEBUG)

            file(MAKE_DIRECTORY ${OUTPUT_PATH})

            execute_process(COMMAND ${Ice_SLICE2${LANG}_EXECUTABLE}
                    ${SLICE_FLAGS_VAR}
                    ${SLICE_PARAMS_VAR}
                    ${SLICE_OPTIONS_VAR}
                    ${SLICE_PARAMS_VAR_OUT}
                    ${SLICE_PARAMS_VAR_OUT_INC}
                    ${slice}
                    WORKING_DIRECTORY ${SLICE_INTERFACES_ROOTDIR}
                    RESULT_VARIABLE result_variable
                    OUTPUT_VARIABLE output_variable
                    ERROR_VARIABLE error_variable
                    OUTPUT_STRIP_TRAILING_WHITESPACE
                    ERROR_STRIP_TRAILING_WHITESPACE
                    )

            if (NOT result_variable EQUAL 0)
                message(WARNING     "________________Generating Ice Files__________")
                message(WARNING     "result_variable = ${result_variable}                  ")
                message(WARNING     "output_variable = ${output_variable}                  ")
                message(WARNING     "error_variable = ${error_variable}                    ")
                message(FATAL_ERROR "________________Generating Ice Files__________")
            endif (NOT result_variable EQUAL 0)

        else (ARG_ON_CONFIG)

            if (ARG_DEBUG)
                message(STATUS "______________________________________________________")
                message(STATUS "WORKING_DIRECTORY \"${SLICE_INTERFACES_ROOTDIR}\" ")
                message(STATUS "COMMAND ${CMAKE_COMMAND} -E make_directory \"${OUTPUT_PATH}\" ")
                message(STATUS "COMMAND ${Ice_SLICE2${LANG}_EXECUTABLE} ")
                message(STATUS "        ${SLICE_FLAGS_VAR} ")
                message(STATUS "        ${SLICE_PARAMS_VAR} ")
                message(STATUS "        ${SLICE_OPTIONS_VAR} ")
                message(STATUS "        ${SLICE_PARAMS_VAR_OUT} ")
                message(STATUS "        ${SLICE_PARAMS_VAR_OUT_INC} ")
                message(STATUS "        ${slice} ")
                message(STATUS "DEPENDS \"${depend_files}\" ")
                message(STATUS "______________________________________________________")
            endif (ARG_DEBUG)

            LIST(APPEND TARGET_CMD_LIST COMMAND ${CMAKE_COMMAND} -E make_directory "${OUTPUT_PATH}")
            LIST(APPEND TARGET_CMD_LIST COMMAND ${Ice_SLICE2${LANG}_EXECUTABLE}
                            ${SLICE_FLAGS_VAR}
                            ${SLICE_PARAMS_VAR}
                            ${SLICE_OPTIONS_VAR}
                            ${SLICE_PARAMS_VAR_OUT}
                            ${SLICE_PARAMS_VAR_OUT_INC}
                            ${slice}
            )

        endif (ARG_ON_CONFIG)

    endforeach()
	
    if (NOT ARG_ON_CONFIG)
        add_custom_target(${TARGET_NAME}
            ${TARGET_CMD_LIST}
            WORKING_DIRECTORY "${SLICE_INTERFACES_ROOTDIR}"
            COMMENT "Processing Ice files..."
        )
		
		add_custom_target(${CLEAN_TARGET_NAME}
				COMMAND ${CMAKE_COMMAND} -D CLEAN_DIR:PATH="${SLICE_OUTPUT_DIR_BASE}" -D CLEAN_EXT="${GEN_FILE_EXT}" -P "${SmartSdk_CMAKE_DIR}/CompileClean.cmake"
				WORKING_DIRECTORY "${SLICE_OUTPUT_DIR_BASE}"
				COMMENT "removing old generated files..."
				)
		add_dependencies(${TARGET_NAME} ${CLEAN_TARGET_NAME})
		
    endif (NOT ARG_ON_CONFIG)

    message(STATUS "Generating Ice files complete")

    if (LANG STREQUAL "CPP")
        set (${OUTPUT_HDR_VAR} ${${OUTPUT_HDR_VAR}} PARENT_SCOPE)
        message(STATUS "OUTPUT_HDR_VAR = ${OUTPUT_HDR_VAR} = ${${OUTPUT_HDR_VAR}} ")

        set (${OUTPUT_CPP_VAR} ${${OUTPUT_CPP_VAR}} PARENT_SCOPE)
        message(STATUS "OUTPUT_CPP_VAR = ${OUTPUT_CPP_VAR} = ${${OUTPUT_CPP_VAR}} ")
    endif (LANG STREQUAL "CPP")


    if (LANG STREQUAL "JS")
        set (${OUTPUT_CPP_VAR} ${${OUTPUT_CPP_VAR}} PARENT_SCOPE)
        message(STATUS "OUTPUT_CPP_VAR = ${OUTPUT_CPP_VAR} = ${${OUTPUT_CPP_VAR}} ")
    endif (LANG STREQUAL "JS")

    if (LANG STREQUAL "JAVA")
        set (${OUTPUT_CPP_VAR} ${${OUTPUT_CPP_VAR}} PARENT_SCOPE)
        message(STATUS "OUTPUT_CPP_VAR = ${OUTPUT_CPP_VAR} = ${${OUTPUT_CPP_VAR}} ")
    endif (LANG STREQUAL "JAVA")

    #Apply compile flags

    if (LANG STREQUAL "CPP")

        unset(ICE_FLAGS)

        include(CheckCXXCompilerFlag)

        foreach(flag ${COMPILE_FLAGS})
            string(REPLACE "/" "_" flag_var "${flag}")
            set(test_cxx_flag "CXX_FLAG${flag_var}")
            CHECK_CXX_COMPILER_FLAG(${flag} "${test_cxx_flag}")
            if (${test_cxx_flag})
                set(ICE_FLAGS "${ICE_FLAGS} ${flag}")
            endif (${test_cxx_flag})
        endforeach(flag ${test_flags})

        set_source_files_properties(
                ${${OUTPUT_CPP_VAR}}
                PROPERTIES
                COMPILE_FLAGS "${ICE_FLAGS}")

    endif (LANG STREQUAL "CPP")

endfunction()


function(GenerateDocumentation)
set(FLAGS      #Flags for generating ice slices.
        ICE        #Allow reserved Ice prefix in Slice identifiers. deprecated: use instead [["ice-prefix"]] metadata.
        UNDERSCORE #Allow underscores in Slice identifiers. deprecated: use instead [["underscore"]] metadata.
        DEBUG      #Print debug messages.
        E          #-E                       Print preprocessor output on stdout.
        )

    set(PARAMS
        ICE_FILES_ROOT #Path to root of Ice interfaces (/some/path/to/file.ice and ICE_FILES "to/file.ice" -> ICE_FILES_ROOT = /some/path/)
        IMAGE_DIR    #--image-dir DIR      Directory containing images for style sheets.
        LOGO_URL     #--logo-url URL       Link to URL from logo image (requires --image-dir).
        OUTPUT_DIR     #--output-dir DIR         Create files in the directory DIR.
        )
    set(OPTIONS
        ICE_INCLUDE_DIRS         #-IDIR      Put DIR in the include file search path.
        DNAME                    #-DNAME     Define NAME as 1.
        UNAME                    #-UNAME     Remove any definition for NAME.
        DNAME_DEF                #-DNAME=DEF Define NAME as DEF.
       )

    cmake_parse_arguments(ARG
            "${FLAGS}"
            "${PARAMS}"
            "${OPTIONS}"
            ${ARGN}
    )


    unset (SLICE_FLAGS_VAR)
    #Checking all Flags;

    if (ARG_ICE)
        set(SLICE_FLAGS_VAR ${SLICE_FLAGS_VAR} --ice)
    endif (ARG_ICE)

    if (ARG_UNDERSCORE)
        set(SLICE_FLAGS_VAR ${SLICE_FLAGS_VAR} --underscore)
    endif (ARG_UNDERSCORE)

    if (ARG_DEBUG)
        set(SLICE_FLAGS_VAR ${SLICE_FLAGS_VAR} --debug)
    endif (ARG_DEBUG)

    if (ARG_E)
        set(SLICE_FLAGS_VAR ${SLICE_FLAGS_VAR} -E)
    endif (ARG_E)

    #Checking all Flags;


    unset(SLICE_PARAMS_VAR)
    unset(SLICE_INTERFACES_ROOTDIR)

    #Checking parameters
    if (ARG_ICE_FILES_ROOT)
        set(SLICE_INTERFACES_ROOTDIR ${ARG_ICE_FILES_ROOT})
    else (ARG_ICE_FILES_ROOT)
        set(SLICE_INTERFACES_ROOTDIR ${CMAKE_SOURCE_DIR}/ice)
        message(STATUS "Not set ICE_FILES_ROOT , search all in \"${CMAKE_CURRENT_LIST_DIR}/..\" ")
    endif (ARG_ICE_FILES_ROOT)

    message(STATUS "SLICE_INTERFACES_ROOTDIR  = ${SLICE_INTERFACES_ROOTDIR}")

    if (ARG_IMAGE_DIR)
        set(SLICE_PARAMS_VAR --image-dir ${ARG_IMAGE_DIR})
    endif (ARG_IMAGE_DIR)

    if (ARG_LOGO_URL)
        if (ARG_IMAGE_DIR)
            set(SLICE_PARAMS_VAR ${SLICE_PARAMS_VAR} --logo-url ${ARG_LOGO_URL})
        else (ARG_IMAGE_DIR)
            message(STATUS "--logo-url requires --image-dir. Ignoring param....")
        endif (ARG_IMAGE_DIR)
    endif (ARG_LOGO_URL)

    if (ARG_OUTPUT_DIR)
        set(SLICE_OUTPUT_DIR ${ARG_OUTPUT_DIR})
    else (ARG_OUTPUT_DIR)
        set(SLICE_OUTPUT_DIR ${CMAKE_BINARY_DIR}/ice_gen)
        message(STATUS "Not set OUTPUT_DIR , generate files to \"${CMAKE_BINARY_DIR}/ice_gen\" ")
    endif (ARG_OUTPUT_DIR)

    if (SLICE_PARAMS_VAR)
        set(SLICE_PARAMS_VAR ${SLICE_PARAMS_VAR} --output-dir ${SLICE_OUTPUT_DIR})
    else(SLICE_PARAMS_VAR)
        set(SLICE_PARAMS_VAR --output-dir ${SLICE_OUTPUT_DIR})
    endif(SLICE_PARAMS_VAR)

    #Checking parameters

    unset(SLICE_OPTIONS_VAR)

    #Checking options
    if (ARG_DNAME)
        foreach (_name ${ARG_DNAME})
            set(SLICE_OPTIONS_VAR ${SLICE_OPTIONS_VAR} -D${_name})
        endforeach()
    endif (ARG_DNAME)

    if (ARG_UNAME)
        foreach (_name ${ARG_UNAME})
            set(SLICE_OPTIONS_VAR ${SLICE_OPTIONS_VAR} -U${_name})
        endforeach()
    endif (ARG_UNAME)

    if (ARG_DNAME_DEF)
        foreach (_name_def ${ARG_DNAME_DEF})
            set(SLICE_OPTIONS_VAR ${SLICE_OPTIONS_VAR} -D${_name_def})
        endforeach()
    endif (ARG_DNAME_DEF)

    if (ARG_ICE_INCLUDE_DIRS)
        foreach (_inc_dir ${ARG_ICE_INCLUDE_DIRS})
            if (SLICE_OPTIONS_VAR)
                set(SLICE_OPTIONS_VAR ${SLICE_OPTIONS_VAR}\ -I${_inc_dir})
            else (SLICE_OPTIONS_VAR)
                set(SLICE_OPTIONS_VAR -I${_inc_dir})
            endif(SLICE_OPTIONS_VAR)

        endforeach()
    endif (ARG_ICE_INCLUDE_DIRS)

    set(SLICE_OPTIONS_VAR ${SLICE_OPTIONS_VAR} -I${Ice_SLICE_DIR})

    #Checking options

    list(LENGTH ICE_FILES FILES_COUNT)
    if (${FILES_COUNT} GREATER 0)

        #generate files.
        execute_process(COMMAND ${Ice_SLICE2HTML_EXECUTABLE}
                ${SLICE_FLAGS_VAR}
                ${SLICE_PARAMS_VAR}
                ${SLICE_OPTIONS_VAR}
                ${ICE_FILES}
                WORKING_DIRECTORY ${SLICE_INTERFACES_ROOTDIR}
                RESULT_VARIABLE result_variable
                OUTPUT_VARIABLE output_variable
                ERROR_VARIABLE error_variable
                OUTPUT_STRIP_TRAILING_WHITESPACE
                ERROR_STRIP_TRAILING_WHITESPACE
                )
        if (NOT result_variable EQUAL 0)
            message(WARNING     "________________Generating Ice Documentation__________")
            message(WARNING     "result_variable = ${result_variable}                  ")
            message(WARNING     "output_variable = ${output_variable}                  ")
            message(WARNING     "error_variable = ${error_variable}                    ")
            message(FATAL_ERROR "________________Generating Ice Documentation__________")
        else (NOT result_variable EQUAL 0)
            message(STATUS "Generating Ice files complete")
        endif (NOT result_variable EQUAL 0)

    endif (${FILES_COUNT} GREATER 0)
endfunction()

set(INTERFACE_SLICE_DIR ${CMAKE_CURRENT_LIST_DIR}/../ice)