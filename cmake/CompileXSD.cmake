cmake_minimum_required(VERSION 3.5.0)

function(CompileXSD outvar)
    set(FLAGS
            DEBUG   #show debug messages
            ON_CONFIG #set flag to generate files at config time. Default false (generate at build time, as target )
            GENERATE_POLYMORPHIC
            POLYMORPHIC_TYPE_ALL
            ORDERED_TYPE_DERIVED
            ORDERED_TYPE_MIXED
            ORDERED_TYPE_ALL
            GENERATE_SERIALIZATION
            GENERATE_OSTREAM
            GENERATE_DOXYGEN
            GENERATE_COMPARISON
            GENERATE_DEFAULT_CTOR
            GENERATE_FROM_BASE_CTOR
            SUPPRESS_ASSIGNMENT
            GENERATE_DETACH
            GENERATE_WILDCARD
            GENERATE_ANY_TYPE
            GENERATE_FORWARD
            SUPPRESS_PARSING
            GENERATE_ELEMENT_TYPE
            GENERATE_ELEMENT_MAP
            GENERATE_INTELLISENSE
            OMIT_DEFAULT_ATTRIBUTES
            NAME_REGEX_TRACE
            ROOT_ELEMENT_FIRST
            ROOT_ELEMENT_LAST
            ROOT_ELEMENT_ALL
            ROOT_ELEMENT_NONE
            GENERATE_INLINE
            GENERATE_XML_SCHEMA
            INCLUDE_WITH_BRACKETS
            INCLUDE_REGEX_TRACE
            EXPORT_XML_SCHEMA
            EXPORT_MAPS
            IMPORT_MAPS
            GENERATE_DEP
            GENERATE_DEP_ONLY
            DEP_PHONY
            PROPRIETARY_LICENSE
            PRESERVE_ANONYMOUS
            SHOW_ANONYMOUS
            ANONYMOUS_REGEX_TRACE
            LOCATION_REGEX_TRACE
            FILE_PER_TYPE
            TYPE_FILE_REGEX_TRACE
            SCHEMA_FILE_REGEX_TRACE
            FAT_TYPE_FILE
            SHOW_SLOC
            NAMESPACE_REGEX_TRACE
            )

    set(PARAMS
            OUTPUT_DIR
            TARGET_NAME    #name of generated target (for next add_dependencies). Default "GenerateXSDClasses"
            ROOT_ELEMENT
			INCLUDE_PREFIX
			STD
			CHAR_TYPE
			CHAR_ENCODING
			HXX_SUFFIX
			IXX_SUFFIX
			CXX_SUFFIX
			FWD_SUFFIX
			PARTS
            GUARD_PREFIX
            HXX_PROLOGUE
            IXX_PROLOGUE
            CXX_PROLOGUE
            HXX_EPILOGUE
            IXX_EPILOGUE
            CXX_EPILOGUE
            DEP_TARGET
            DISABLE_WARNING
            OPTIONS_FILE
            )
    set(OPTIONS
            INPUT_FILES			# input schemas
            NAMESPACE_MAP
            RESERVED_NAME
            LOCATION_MAP
            )

    cmake_parse_arguments(ARG
            "${FLAGS}"
            "${PARAMS}"
            "${OPTIONS}"
            ${ARGN}
    )

###########################################################################
#  Proccess flags.
###########################################################################
    if (ARG_TARGET_NAME)
        set(TARGET_NAME ${ARG_TARGET_NAME})
    else (ARG_TARGET_NAME)
        set(TARGET_NAME GenerateXSDClasses )
    endif (ARG_TARGET_NAME)


    if ( ARG_GENERATE_POLYMORPHIC )
        list(APPEND FLAG_LIST --generate-polymorphic )
    endif ( ARG_GENERATE_POLYMORPHIC )

    if ( ARG_POLYMORPHIC_TYPE_ALL )
        list(APPEND FLAG_LIST --polymorphic-type-all )
    endif ( ARG_POLYMORPHIC_TYPE_ALL )

    if ( ARG_ORDERED_TYPE_DERIVED )
        list(APPEND FLAG_LIST --ordered-type-derived )
    endif ( ARG_ORDERED_TYPE_DERIVED )

    if ( ARG_ORDERED_TYPE_MIXED )
        list(APPEND FLAG_LIST --ordered-type-mixed )
    endif ( ARG_ORDERED_TYPE_MIXED )

    if ( ARG_ORDERED_TYPE_ALL )
        list(APPEND FLAG_LIST --ordered-type-all )
    endif ( ARG_ORDERED_TYPE_ALL )

    if ( ARG_GENERATE_SERIALIZATION )
        list(APPEND FLAG_LIST --generate-serialization )
    endif ( ARG_GENERATE_SERIALIZATION )

    if ( ARG_GENERATE_OSTREAM )
        list(APPEND FLAG_LIST --generate-ostream )
    endif ( ARG_GENERATE_OSTREAM )

    if ( ARG_GENERATE_DOXYGEN )
        list(APPEND FLAG_LIST --generate-doxygen )
    endif ( ARG_GENERATE_DOXYGEN )

    if ( ARG_GENERATE_COMPARISON )
        list(APPEND FLAG_LIST --generate-comparison )
    endif ( ARG_GENERATE_COMPARISON )

    if ( ARG_GENERATE_DEFAULT_CTOR )
        list(APPEND FLAG_LIST --generate-default-ctor )
    endif ( ARG_GENERATE_DEFAULT_CTOR )

    if ( ARG_GENERATE_FROM_BASE_CTOR )
        list(APPEND FLAG_LIST --generate-from-base-ctor )
    endif ( ARG_GENERATE_FROM_BASE_CTOR )

    if ( ARG_SUPPRESS_ASSIGNMENT )
        list(APPEND FLAG_LIST --suppress-assignment )
    endif ( ARG_SUPPRESS_ASSIGNMENT )

    if ( ARG_GENERATE_DETACH )
        list(APPEND FLAG_LIST --generate-detach )
    endif ( ARG_GENERATE_DETACH )

    if ( ARG_GENERATE_WILDCARD )
        list(APPEND FLAG_LIST --generate-wildcard )
    endif ( ARG_GENERATE_WILDCARD )

    if ( ARG_GENERATE_ANY_TYPE )
        list(APPEND FLAG_LIST --generate-any-type )
    endif ( ARG_GENERATE_ANY_TYPE )

    if ( ARG_GENERATE_FORWARD )
        list(APPEND FLAG_LIST --generate-forward )
    endif ( ARG_GENERATE_FORWARD )

    if ( ARG_SUPPRESS_PARSING )
        list(APPEND FLAG_LIST --suppress-parsing )
    endif ( ARG_SUPPRESS_PARSING )

    if ( ARG_GENERATE_ELEMENT_TYPE )
        list(APPEND FLAG_LIST --generate-element-type )
    endif ( ARG_GENERATE_ELEMENT_TYPE )

    if ( ARG_GENERATE_ELEMENT_MAP )
        list(APPEND FLAG_LIST --generate-element-map )
    endif ( ARG_GENERATE_ELEMENT_MAP )

    if ( ARG_GENERATE_INTELLISENSE )
        list(APPEND FLAG_LIST --generate-intellisense )
    endif ( ARG_GENERATE_INTELLISENSE )

    if ( ARG_OMIT_DEFAULT_ATTRIBUTES )
        list(APPEND FLAG_LIST --omit-default-attributes )
    endif ( ARG_OMIT_DEFAULT_ATTRIBUTES )

    if ( ARG_NAME_REGEX_TRACE )
        list(APPEND FLAG_LIST --name-regex-trace )
    endif ( ARG_NAME_REGEX_TRACE )

    if ( ARG_ROOT_ELEMENT_FIRST )
        list(APPEND FLAG_LIST --root-element-first )
    endif ( ARG_ROOT_ELEMENT_FIRST )

    if ( ARG_ROOT_ELEMENT_LAST )
        list(APPEND FLAG_LIST --root-element-last )
    endif ( ARG_ROOT_ELEMENT_LAST )

    if ( ARG_ROOT_ELEMENT_ALL )
        list(APPEND FLAG_LIST --root-element-all )
    endif ( ARG_ROOT_ELEMENT_ALL )

    if ( ARG_ROOT_ELEMENT_NONE )
        list(APPEND FLAG_LIST --root-element-none )
    endif ( ARG_ROOT_ELEMENT_NONE )

    if ( ARG_GENERATE_INLINE )
        list(APPEND FLAG_LIST --generate-inline )
    endif ( ARG_GENERATE_INLINE )

    if ( ARG_GENERATE_XML_SCHEMA )
        list(APPEND FLAG_LIST --generate-xml-schema )
    endif ( ARG_GENERATE_XML_SCHEMA )

    if ( ARG_INCLUDE_WITH_BRACKETS )
        list(APPEND FLAG_LIST --include-with-brackets )
    endif ( ARG_INCLUDE_WITH_BRACKETS )

    if ( ARG_INCLUDE_REGEX_TRACE )
        list(APPEND FLAG_LIST --include-regex-trace )
    endif ( ARG_INCLUDE_REGEX_TRACE )

    if ( ARG_EXPORT_XML_SCHEMA )
        list(APPEND FLAG_LIST --export-xml-schema )
    endif ( ARG_EXPORT_XML_SCHEMA )

    if ( ARG_EXPORT_MAPS )
        list(APPEND FLAG_LIST --export-maps )
    endif ( ARG_EXPORT_MAPS )

    if ( ARG_IMPORT_MAPS )
        list(APPEND FLAG_LIST --import-maps )
    endif ( ARG_IMPORT_MAPS )

    if ( ARG_GENERATE_DEP )
        list(APPEND FLAG_LIST --generate-dep )
    endif ( ARG_GENERATE_DEP )

    if ( ARG_GENERATE_DEP_ONLY )
        list(APPEND FLAG_LIST --generate-dep-only )
    endif ( ARG_GENERATE_DEP_ONLY )

    if ( ARG_DEP_PHONY )
        list(APPEND FLAG_LIST --dep-phony )
    endif ( ARG_DEP_PHONY )

    if ( ARG_PROPRIETARY_LICENSE )
        list(APPEND FLAG_LIST --proprietary-license )
    endif ( ARG_PROPRIETARY_LICENSE )

    if ( ARG_PRESERVE_ANONYMOUS )
        list(APPEND FLAG_LIST --preserve-anonymous )
    endif ( ARG_PRESERVE_ANONYMOUS )

    if ( ARG_SHOW_ANONYMOUS )
        list(APPEND FLAG_LIST --show-anonymous )
    endif ( ARG_SHOW_ANONYMOUS )

    if ( ARG_ANONYMOUS_REGEX_TRACE )
        list(APPEND FLAG_LIST --anonymous-regex-trace )
    endif ( ARG_ANONYMOUS_REGEX_TRACE )

    if ( ARG_LOCATION_REGEX_TRACE )
        list(APPEND FLAG_LIST --location-regex-trace )
    endif ( ARG_LOCATION_REGEX_TRACE )

    if ( ARG_FILE_PER_TYPE )
        list(APPEND FLAG_LIST --file-per-type )
    endif ( ARG_FILE_PER_TYPE )

    if ( ARG_TYPE_FILE_REGEX_TRACE )
        list(APPEND FLAG_LIST --type-file-regex-trace )
    endif ( ARG_TYPE_FILE_REGEX_TRACE )

    if ( ARG_SCHEMA_FILE_REGEX_TRACE )
        list(APPEND FLAG_LIST --schema-file-regex-trace )
    endif ( ARG_SCHEMA_FILE_REGEX_TRACE )

    if ( ARG_FAT_TYPE_FILE )
        list(APPEND FLAG_LIST --fat-type-file )
    endif ( ARG_FAT_TYPE_FILE )

    if ( ARG_SHOW_SLOC )
        list(APPEND FLAG_LIST --show-sloc )
    endif ( ARG_SHOW_SLOC )

    if ( ARG_NAMESPACE_REGEX_TRACE )
        list(APPEND FLAG_LIST --namespace-regex-trace )
    endif ( ARG_NAMESPACE_REGEX_TRACE )


###########################################################################
#  Proccess flags.
###########################################################################



###########################################################################
#  Proccess params.
###########################################################################

    if ( ARG_OUTPUT_DIR )
        LIST(APPEND PARAMS_LIST --output-dir ${ARG_OUTPUT_DIR} )
    endif ( ARG_OUTPUT_DIR)

    if ( ARG_POLYMORPHIC_TYPE )
        LIST(APPEND PARAMS_LIST --polymorphic-type ${ARG_POLYMORPHIC_TYPE} )
    endif ( ARG_POLYMORPHIC_TYPE )

    if ( ARG_POLYMORPHIC_PLATE )
        LIST(APPEND PARAMS_LIST --polymorphic-plate ${ARG_POLYMORPHIC_PLATE} )
    endif ( ARG_POLYMORPHIC_PLATE )

    if ( ARG_ORDERED_TYPE )
        LIST(APPEND PARAMS_LIST --ordered-type ${ARG_ORDERED_TYPE} )
    endif ( ARG_ORDERED_TYPE )

    if ( ARG_ORDER_CONTAINER )
        LIST(APPEND PARAMS_LIST --order-container ${ARG_ORDER_CONTAINER} )
    endif ( ARG_ORDER_CONTAINER )

    if ( ARG_GENERATE_INSERTION )
        LIST(APPEND PARAMS_LIST --generate-insertion ${ARG_GENERATE_INSERTION} )
    endif ( ARG_GENERATE_INSERTION )

    if ( ARG_GENERATE_EXTRACTION )
        LIST(APPEND PARAMS_LIST --generate-extraction ${ARG_GENERATE_EXTRACTION} )
    endif ( ARG_GENERATE_EXTRACTION )

    if ( ARG_TYPE_NAMING )
        LIST(APPEND PARAMS_LIST --type-naming ${ARG_TYPE_NAMING} )
    endif ( ARG_TYPE_NAMING )

    if ( ARG_FUNCTION_NAMING )
        LIST(APPEND PARAMS_LIST --function-naming ${ARG_FUNCTION_NAMING} )
    endif ( ARG_FUNCTION_NAMING )

    if ( ARG_TYPE_REGEX )
        LIST(APPEND PARAMS_LIST --type-regex ${ARG_TYPE_REGEX} )
    endif ( ARG_TYPE_REGEX )

    if ( ARG_ACCESSOR_REGEX )
        LIST(APPEND PARAMS_LIST --accessor-regex ${ARG_ACCESSOR_REGEX} )
    endif ( ARG_ACCESSOR_REGEX )

    if ( ARG_ONE_ACCESSOR_REGEX )
        LIST(APPEND PARAMS_LIST --one-accessor-regex ${ARG_ONE_ACCESSOR_REGEX} )
    endif ( ARG_ONE_ACCESSOR_REGEX )

    if ( ARG_OPT_ACCESSOR_REGEX )
        LIST(APPEND PARAMS_LIST --opt-accessor-regex ${ARG_OPT_ACCESSOR_REGEX} )
    endif ( ARG_OPT_ACCESSOR_REGEX )

    if ( ARG_SEQ_ACCESSOR_REGEX )
        LIST(APPEND PARAMS_LIST --seq-accessor-regex ${ARG_SEQ_ACCESSOR_REGEX} )
    endif ( ARG_SEQ_ACCESSOR_REGEX )

    if ( ARG_MODIFIER_REGEX )
        LIST(APPEND PARAMS_LIST --modifier-regex ${ARG_MODIFIER_REGEX} )
    endif ( ARG_MODIFIER_REGEX )

    if ( ARG_ONE_MODIFIER_REGEX )
        LIST(APPEND PARAMS_LIST --one-modifier-regex ${ARG_ONE_MODIFIER_REGEX} )
    endif ( ARG_ONE_MODIFIER_REGEX )

    if ( ARG_OPT_MODIFIER_REGEX )
        LIST(APPEND PARAMS_LIST --opt-modifier-regex ${ARG_OPT_MODIFIER_REGEX} )
    endif ( ARG_OPT_MODIFIER_REGEX )

    if ( ARG_SEQ_MODIFIER_REGEX )
        LIST(APPEND PARAMS_LIST --seq-modifier-regex ${ARG_SEQ_MODIFIER_REGEX} )
    endif ( ARG_SEQ_MODIFIER_REGEX )

    if ( ARG_PARSER_REGEX )
        LIST(APPEND PARAMS_LIST --parser-regex ${ARG_PARSER_REGEX} )
    endif ( ARG_PARSER_REGEX )

    if ( ARG_SERIALIZER_REGEX )
        LIST(APPEND PARAMS_LIST --serializer-regex ${ARG_SERIALIZER_REGEX} )
    endif ( ARG_SERIALIZER_REGEX )

    if ( ARG_CONST_REGEX )
        LIST(APPEND PARAMS_LIST --const-regex ${ARG_CONST_REGEX} )
    endif ( ARG_CONST_REGEX )

    if ( ARG_ENUMERATOR_REGEX )
        LIST(APPEND PARAMS_LIST --enumerator-regex ${ARG_ENUMERATOR_REGEX} )
    endif ( ARG_ENUMERATOR_REGEX )

    if ( ARG_ELEMENT_TYPE_REGEX )
        LIST(APPEND PARAMS_LIST --element-type-regex ${ARG_ELEMENT_TYPE_REGEX} )
    endif ( ARG_ELEMENT_TYPE_REGEX )

    if ( ARG_CUSTOM_TYPE )
        LIST(APPEND PARAMS_LIST --custom-type ${ARG_CUSTOM_TYPE} )
    endif ( ARG_CUSTOM_TYPE )

    if ( ARG_CUSTOM_TYPE_REGEX )
        LIST(APPEND PARAMS_LIST --custom-type-regex ${ARG_CUSTOM_TYPE_REGEX} )
    endif ( ARG_CUSTOM_TYPE_REGEX )

    if ( ARG_PARTS_SUFFIX )
        LIST(APPEND PARAMS_LIST --parts-suffix ${ARG_PARTS_SUFFIX} )
    endif ( ARG_PARTS_SUFFIX )

    if ( ARG_STD )
        LIST(APPEND PARAMS_LIST --std ${ARG_STD} )
    endif ( ARG_STD )

    if ( ARG_CHAR_TYPE )
        LIST(APPEND PARAMS_LIST --char-type ${ARG_CHAR_TYPE} )
    endif ( ARG_CHAR_TYPE )

    if ( ARG_CHAR_ENCODING )
        LIST(APPEND PARAMS_LIST --char-encoding ${ARG_CHAR_ENCODING} )
    endif ( ARG_CHAR_ENCODING )

    if ( ARG_EXTERN_XML_SCHEMA )
        LIST(APPEND PARAMS_LIST --extern-xml-schema ${ARG_EXTERN_XML_SCHEMA} )
    endif ( ARG_EXTERN_XML_SCHEMA )

    if ( ARG_NAMESPACE_REGEX )
        LIST(APPEND PARAMS_LIST --namespace-regex ${ARG_NAMESPACE_REGEX} )
    endif ( ARG_NAMESPACE_REGEX )

    if ( ARG_INCLUDE_PREFIX )
        LIST(APPEND PARAMS_LIST --include-prefix ${ARG_INCLUDE_PREFIX} )
    endif ( ARG_INCLUDE_PREFIX )

    if ( ARG_INCLUDE_REGEX )
        LIST(APPEND PARAMS_LIST --include-regex ${ARG_INCLUDE_REGEX} )
    endif ( ARG_INCLUDE_REGEX )

    if ( ARG_HXX_SUFFIX )
        LIST(APPEND PARAMS_LIST --hxx-suffix ${ARG_HXX_SUFFIX} )
    endif ( ARG_HXX_SUFFIX )

    if ( ARG_IXX_SUFFIX )
        LIST(APPEND PARAMS_LIST --ixx-suffix ${ARG_IXX_SUFFIX} )
    endif ( ARG_IXX_SUFFIX )

    if ( ARG_CXX_SUFFIX )
        LIST(APPEND PARAMS_LIST --cxx-suffix ${ARG_CXX_SUFFIX} )
    endif ( ARG_CXX_SUFFIX )

    if ( ARG_FWD_SUFFIX )
        LIST(APPEND PARAMS_LIST --fwd-suffix ${ARG_FWD_SUFFIX} )
    endif ( ARG_FWD_SUFFIX )

    if ( ARG_HXX_REGEX )
        LIST(APPEND PARAMS_LIST --hxx-regex ${ARG_HXX_REGEX} )
    endif ( ARG_HXX_REGEX )

    if ( ARG_IXX_REGEX )
        LIST(APPEND PARAMS_LIST --ixx-regex ${ARG_IXX_REGEX} )
    endif ( ARG_IXX_REGEX )

    if ( ARG_CXX_REGEX )
        LIST(APPEND PARAMS_LIST --cxx-regex ${ARG_CXX_REGEX} )
    endif ( ARG_CXX_REGEX )

    if ( ARG_FWD_REGEX )
        LIST(APPEND PARAMS_LIST --fwd-regex ${ARG_FWD_REGEX} )
    endif ( ARG_FWD_REGEX )

    if ( ARG_FWD_PROLOGUE )
        LIST(APPEND PARAMS_LIST --fwd-prologue ${ARG_FWD_PROLOGUE} )
    endif ( ARG_FWD_PROLOGUE )

    if ( ARG_PROLOGUE )
        LIST(APPEND PARAMS_LIST --prologue ${ARG_PROLOGUE} )
    endif ( ARG_PROLOGUE )

    if ( ARG_FWD_EPILOGUE )
        LIST(APPEND PARAMS_LIST --fwd-epilogue ${ARG_FWD_EPILOGUE} )
    endif ( ARG_FWD_EPILOGUE )

    if ( ARG_EPILOGUE )
        LIST(APPEND PARAMS_LIST --epilogue ${ARG_EPILOGUE} )
    endif ( ARG_EPILOGUE )

    if ( ARG_HXX_PROLOGUE_FILE )
        LIST(APPEND PARAMS_LIST --hxx-prologue-file ${ARG_HXX_PROLOGUE_FILE} )
    endif ( ARG_HXX_PROLOGUE_FILE )

    if ( ARG_IXX_PROLOGUE_FILE )
        LIST(APPEND PARAMS_LIST --ixx-prologue-file ${ARG_IXX_PROLOGUE_FILE} )
    endif ( ARG_IXX_PROLOGUE_FILE )

    if ( ARG_CXX_PROLOGUE_FILE )
        LIST(APPEND PARAMS_LIST --cxx-prologue-file ${ARG_CXX_PROLOGUE_FILE} )
    endif ( ARG_CXX_PROLOGUE_FILE )

    if ( ARG_FWD_PROLOGUE_FILE )
        LIST(APPEND PARAMS_LIST --fwd-prologue-file ${ARG_FWD_PROLOGUE_FILE} )
    endif ( ARG_FWD_PROLOGUE_FILE )

    if ( ARG_PROLOGUE_FILE )
        LIST(APPEND PARAMS_LIST --prologue-file ${ARG_PROLOGUE_FILE} )
    endif ( ARG_PROLOGUE_FILE )

    if ( ARG_HXX_EPILOGUE_FILE )
        LIST(APPEND PARAMS_LIST --hxx-epilogue-file ${ARG_HXX_EPILOGUE_FILE} )
    endif ( ARG_HXX_EPILOGUE_FILE )

    if ( ARG_IXX_EPILOGUE_FILE )
        LIST(APPEND PARAMS_LIST --ixx-epilogue-file ${ARG_IXX_EPILOGUE_FILE} )
    endif ( ARG_IXX_EPILOGUE_FILE )

    if ( ARG_CXX_EPILOGUE_FILE )
        LIST(APPEND PARAMS_LIST --cxx-epilogue-file ${ARG_CXX_EPILOGUE_FILE} )
    endif ( ARG_CXX_EPILOGUE_FILE )

    if ( ARG_FWD_EPILOGUE_FILE )
        LIST(APPEND PARAMS_LIST --fwd-epilogue-file ${ARG_FWD_EPILOGUE_FILE} )
    endif ( ARG_FWD_EPILOGUE_FILE )

    if ( ARG_EPILOGUE_FILE )
        LIST(APPEND PARAMS_LIST --epilogue-file ${ARG_EPILOGUE_FILE} )
    endif ( ARG_EPILOGUE_FILE )

    if ( ARG_EXPORT_SYMBOL )
        LIST(APPEND PARAMS_LIST --export-symbol ${ARG_EXPORT_SYMBOL} )
    endif ( ARG_EXPORT_SYMBOL )

    if ( ARG_DEP_SUFFIX )
        LIST(APPEND PARAMS_LIST --dep-suffix ${ARG_DEP_SUFFIX} )
    endif ( ARG_DEP_SUFFIX )

    if ( ARG_DEP_REGEX )
        LIST(APPEND PARAMS_LIST --dep-regex ${ARG_DEP_REGEX} )
    endif ( ARG_DEP_REGEX )

    if ( ARG_SLOC_LIMIT )
        LIST(APPEND PARAMS_LIST --sloc-limit ${ARG_SLOC_LIMIT} )
    endif ( ARG_SLOC_LIMIT )

    if ( ARG_CUSTOM_LITERALS )
        LIST(APPEND PARAMS_LIST --custom-literals ${ARG_CUSTOM_LITERALS} )
    endif ( ARG_CUSTOM_LITERALS )

    if ( ARG_ANONYMOUS_REGEX )
        LIST(APPEND PARAMS_LIST --anonymous-regex ${ARG_ANONYMOUS_REGEX} )
    endif ( ARG_ANONYMOUS_REGEX )

    if ( ARG_LOCATION_REGEX )
        LIST(APPEND PARAMS_LIST --location-regex ${ARG_LOCATION_REGEX} )
    endif ( ARG_LOCATION_REGEX )

    if ( ARG_TYPE_FILE_REGEX )
        LIST(APPEND PARAMS_LIST --type-file-regex ${ARG_TYPE_FILE_REGEX} )
    endif ( ARG_TYPE_FILE_REGEX )

    if ( ARG_SCHEMA_FILE_REGEX )
        LIST(APPEND PARAMS_LIST --schema-file-regex ${ARG_SCHEMA_FILE_REGEX} )
    endif ( ARG_SCHEMA_FILE_REGEX )

    if ( ARG_FILE_LIST_DELIM )
        LIST(APPEND PARAMS_LIST --file-list-delim ${ARG_FILE_LIST_DELIM} )
    endif ( ARG_FILE_LIST_DELIM )

    if ( ARG_ROOT_ELEMENT )
        LIST(APPEND PARAMS_LIST --root-element ${ARG_ROOT_ELEMENT} )
    endif ( ARG_ROOT_ELEMENT )

    if ( ARG_PARTS )
        LIST(APPEND PARAMS_LIST --parts ${ARG_PARTS} )
    endif ( ARG_PARTS )

    if ( ARG_GUARD_PREFIX )
        LIST(APPEND PARAMS_LIST --guard-prefix ${ARG_GUARD_PREFIX} )
    endif ( ARG_GUARD_PREFIX )

    if ( ARG_HXX_PROLOGUE )
        LIST(APPEND PARAMS_LIST --hxx-prologue ${ARG_HXX_PROLOGUE} )
    endif ( ARG_HXX_PROLOGUE )

    if ( ARG_IXX_PROLOGUE )
        LIST(APPEND PARAMS_LIST --ixx-prologue ${ARG_IXX_PROLOGUE} )
    endif ( ARG_IXX_PROLOGUE )

    if ( ARG_CXX_PROLOGUE )
        LIST(APPEND PARAMS_LIST --cxx-prologue ${ARG_CXX_PROLOGUE} )
    endif ( ARG_CXX_PROLOGUE )

    if ( ARG_HXX_EPILOGUE )
        LIST(APPEND PARAMS_LIST --hxx-epilogue ${ARG_HXX_EPILOGUE} )
    endif ( ARG_HXX_EPILOGUE )

    if ( ARG_IXX_EPILOGUE )
        LIST(APPEND PARAMS_LIST --ixx-epilogue ${ARG_IXX_EPILOGUE} )
    endif ( ARG_IXX_EPILOGUE )

    if ( ARG_CXX_EPILOGUE )
        LIST(APPEND PARAMS_LIST --cxx-epilogue ${ARG_CXX_EPILOGUE} )
    endif ( ARG_CXX_EPILOGUE )

    if ( ARG_DEP_TARGET )
        LIST(APPEND PARAMS_LIST --dep-target ${ARG_DEP_TARGET} )
    endif ( ARG_DEP_TARGET )

    if ( ARG_DISABLE_WARNING )
        LIST(APPEND PARAMS_LIST --disable-warning ${ARG_DISABLE_WARNING} )
    endif ( ARG_DISABLE_WARNING )

    if ( ARG_OPTIONS_FILE )
        LIST(APPEND PARAMS_LIST --options-file ${ARG_OPTIONS_FILE} )
    endif ( ARG_OPTIONS_FILE )

###########################################################################
#  Proccess params.
###########################################################################



###########################################################################
#  Proccess options.
###########################################################################

    if ( ARG_NAMESPACE_MAP )
        LIST( APPEND OPTIONS_LIST --namespace-map ${ARG_NAMESPACE_MAP} )
    endif ( ARG_NAMESPACE_MAP )

    if ( ARG_RESERVED_NAME )
        LIST( APPEND OPTIONS_LIST --reserved-name ${ARG_RESERVED_NAME} )
    endif ( ARG_RESERVED_NAME )

    if ( ARG_LOCATION_MAP )
        LIST( APPEND OPTIONS_LIST --location-map ${ARG_LOCATION_MAP} )
    endif ( ARG_LOCATION_MAP )


###########################################################################
#  Proccess options.
###########################################################################

    if ( NOT ARG_INPUT_FILES )
        message(FATAL_ERROR "Empty file list! ")
    endif ( NOT ARG_INPUT_FILES )

	if ( ARG_OUTPUT_DIR )
		set(GENERATED_FILELIST ${ARG_OUTPUT_DIR}/generated.list)
	else()
		set(GENERATED_FILELIST generated.list)
	endif()
	
	LIST(APPEND PARAMS_LIST --file-list ${GENERATED_FILELIST} )
	
    if (ARG_ON_CONFIG)

        if ( ARG_DEBUG )
            message(STATUS "______________________________________________________")
            message(STATUS "execute_process( COMMAND ${XSD_EXECUTABLE} " )
            message(STATUS "        cxx-tree " )
            message(STATUS "        ${FLAG_LIST} " )
            message(STATUS "        ${PARAMS_LIST} " )
            message(STATUS "        ${OPTIONS_LIST} " )
            message(STATUS "        ${ARG_INPUT_FILES} " )
            message(STATUS "        RESULT_VARIABLE result_variable " )
            message(STATUS "        OUTPUT_VARIABLE output_variable " )
            message(STATUS "        ERROR_VARIABLE error_variable " )
            message(STATUS "        OUTPUT_STRIP_TRAILING_WHITESPACE " )
            message(STATUS "        ERROR_STRIP_TRAILING_WHITESPACE " )
            message(STATUS "        ) " )
            message(STATUS "______________________________________________________")
        endif ( ARG_DEBUG )


        # foreach XSD file generate C++ binding files
        execute_process( 
			COMMAND 
						"${XSD_EXECUTABLE}"
						cxx-tree
						${FLAG_LIST}
						${PARAMS_LIST}
						${OPTIONS_LIST}
						${ARG_INPUT_FILES}
			RESULT_VARIABLE result_variable
			OUTPUT_VARIABLE output_variable
			ERROR_VARIABLE error_variable
			OUTPUT_STRIP_TRAILING_WHITESPACE
			ERROR_STRIP_TRAILING_WHITESPACE
			)

        if ( NOT result_variable EQUAL 0 )
            message(WARNING     "________________Generating xsd Files__________")
            message(WARNING     "result_variable = ${result_variable}                  ")
            message(WARNING     "output_variable = ${output_variable}                  ")
            message(WARNING     "error_variable = ${error_variable}                    ")
            message(WARNING     "________________Generating xsd Files__________")
        endif ( NOT result_variable EQUAL 0 )

    else (ARG_ON_CONFIG)

        if ( ARG_DEBUG )
            message(STATUS "______________________________________________________")
            message(STATUS "add_custom_target(${TARGET_NAME}" )
            message(STATUS "        COMMAND ${XSD_EXECUTABLE}" )
            message(STATUS "                    cxx-tree" )
            message(STATUS "                    ${FLAG_LIST}" )
            message(STATUS "                    ${PARAMS_LIST}" )
            message(STATUS "                    ${OPTIONS_LIST}" )
            message(STATUS "                    ${ARG_INPUT_FILES}" )
            message(STATUS "        COMMENT \"Generate from xsd files...\"" )
            message(STATUS "        )" )
            message(STATUS "______________________________________________________")
        endif ( ARG_DEBUG )

        add_custom_target(${TARGET_NAME}
                COMMAND 
					"${XSD_EXECUTABLE}"
					cxx-tree
					${FLAG_LIST}
					${PARAMS_LIST}
					${OPTIONS_LIST}
					${ARG_INPUT_FILES}
                COMMENT "Generate from xsd files..."
                )

    endif (ARG_ON_CONFIG)

	if(EXISTS ${GENERATED_FILELIST})
	
		file(STRINGS ${GENERATED_FILELIST} RAW_FILES )
		file(REMOVE ${GENERATED_FILELIST})
		
		set(${outvar})
		foreach(f ${RAW_FILES})
			file(TO_CMAKE_PATH ${f} fc)
			list(APPEND ${outvar} ${fc})
		endforeach()	
		
		set(${outvar} ${${outvar}} PARENT_SCOPE)
	else()
		message(WARNING "XSD File List is EMPTY")
	endif()
	
endfunction()


