
# GenerateFind (
#
#    NO_DEFAULT_PATH append to config file option NO_DEFAULT_PATH
#
#    PRJ        Name of Project ( Find${PRJ}.cmake  )
#    GEN_DIR    directory for generated config FindFile ( ${ROOT}/ )
#    INST_DIR   directory for install config FindFile ( DESTINATION ${CMAKE_INSTALL_PREFIX}/cmake )
#
#    ROOT_VAR   root dir of PRJ
#    ROOTDIR    root dir of PRJ
#
#    BIN_FILES  list of files to search
#    INC_FILES  list of files to search
#    LIB_FILES  list of files to search
#
#    PKGS       list of package names to search ( openssl, ssl , ogg , etc )
#    PKGS_FILES list of package files to search in PKG_CONFIG_PATH ( openssl.ps , ssl.ps , ogg.ps , etc )
#
#    BIN_DIRS   additonal paths for search binaries
#    INC_DIRS   additonal paths for search headers
#    LIB_DIRS   additonal paths for search libraries
#    PKG_DIRS   additonal paths for including to PKG_CONFIG_PATH
#
#    Example of usage:
#    GenerateFind( PRJ ZLIB
#                  GEN_DIR ${ROOT}
#                  INST_DIR ${CMAKE_INSTALL_PREFIX}/cmake
#    )
#
#    GenerateFind( PRJ ZLIB
#                  GEN_DIR ${ROOT}
#                  INST_DIR ${CMAKE_INSTALL_PREFIX}/cmake
#    )

function(GenerateFind)

    #set up input arguments

    #if TARGET is set add custom_target instead of ExternalProject_add
    set(Option NO_DEFAULT_PATH
            )
    #ProjectName , ProjectVersion , Variable to Add CMAKE_PREFIX_PATH , to add PATH , PKG_CONFIG_PATH
    #script add variable SOME_VAR_ORIG , and it must contains original value
    set(OneValueArgs
            PRJ         #Name of Project ( Find${PRJ}.cmake  )
            GEN_DIR     #directory for generated config FindFile ( ${ROOT}/ )
            INST_DIR    #directory for install config FindFile ( DESTINATION ${CMAKE_INSTALL_PREFIX}/cmake )
            ROOT_VAR    #root dir of PRJ
            ROOTDIR     #root dir of PRJ
            )
    #Variables to set and unset
    set(multiValueArgs
            BIN_FILES   #list of files to search
            INC_FILES   #list of files to search
            LIB_FILES   #list of files to search
            BIN_DIRS    #additonal paths for search binaries
            INC_DIRS    #additonal paths for search headers
            LIB_DIRS    #additonal paths for search libraries
            PKG_DIRS    #additonal paths for including to PKG_CONFIG_PATH
            PKGS        #list of package names to search ( openssl, ssl , ogg , etc )
            PKGS_FILES  #list of package files to search in PKG_CONFIG_PATH ( openssl.pc , ssl.pc , ogg.pc , etc )
            )

    cmake_parse_arguments(  #Parse arguments
            PARAM # prefix of output variables
            "${Option}" # list of names of the boolean arguments (only defined ones will be true)
            "${OneValueArgs}" # list of names of mono-valued arguments
            "${multiValueArgs}" # list of names of multi-valued arguments (output variables are lists)
            ${ARGN} # arguments of the function to parse, here we take the all original ones
            )

    if (NOT PARAM_PRJ)
        MESSAGE(FATAL_ERROR "Parameter PRJ not set!" )
    endif (NOT PARAM_PRJ)


    if (NOT PARAM_BIN_FILES AND NOT PARAM_INC_FILES AND NOT PARAM_LIB_FILES)
        MESSAGE(FATAL_ERROR "not specified files to search" )
    endif (NOT PARAM_BIN_FILES AND NOT PARAM_INC_FILES AND NOT PARAM_LIB_FILES)


    if (PARAM_GEN_DIR)
        set( GEN_DIR ${PARAM_GEN_DIR})
    else (PARAM_GEN_DIR)
        set( GEN_DIR ${CMAKE_CURRENT_BINARY_DIR})
    endif (PARAM_GEN_DIR)

    if (PARAM_INST_DIR)
        set( INST_DIR ${PARAM_INST_DIR})
    else (PARAM_INST_DIR)
        set( INST_DIR ${CMAKE_INSTALL_PREFIX}/cmake )
    endif (PARAM_INST_DIR)

    set(bin_files ${PARAM_BIN_FILES})
    set(header_files ${PARAM_INC_FILES})
    set(lib_files ${PARAM_LIB_FILES})

    set(pkg_names ${PARAM_PKGS})
    set(pkg_files ${PARAM_PKGS_FILES})

    set(BIN_DIRS ${PARAM_BIN_DIRS})
    set(INC_DIRS ${PARAM_INC_DIRS})
    set(LIB_DIRS ${PARAM_LIB_DIRS})
    set(PKG_DIRS ${PARAM_PKG_DIRS})

    if ( PARAM_ROOT_VAR )
        set(${PARAM_PRJ}_ROOTDIR ${PARAM_ROOT_VAR} )
    endif ( PARAM_ROOT_VAR )
    if ( PARAM_ROOTDIR )
        set( ROOT_VAR ${PARAM_ROOTDIR} )

    endif ( PARAM_ROOTDIR )

    if ( PARAM_NO_DEFAULT_PATH )
        set( DEFAULT_PATH NO_DEFAULT_PATH )
    endif ( PARAM_NO_DEFAULT_PATH )

    #configure underlying file Find${PRJ}.cmake for this project
    configure_file( ${CMAKE_SOURCE_DIR}/cmake/TemplateFileFind.cmake.in
            ${GEN_DIR}/Find${PARAM_PRJ}.cmake @ONLY )

    install(FILES       ${GEN_DIR}/Find${PARAM_PRJ}.cmake
            DESTINATION ${INST_DIR}/ )

endfunction(GenerateFind)

