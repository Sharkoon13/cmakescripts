
# GenerateConfigFile (
#    FILE  filPath
#
#    REMOVE   list of variables to UNSET ( will be unset all ( cache , scoped , enviroment) )
#
#    SCOPED list of scoped variables with next Format ( var1;val1;var2;val2  etc.)
#    ENVS   list of enviroment variables with next Format ( var1;val1;var2;val2  etc.)
#    CACHED list of cached variables with next Format ( var1;val1;var2;val2  etc.)
# )

function(GenerateConfigFile)

    #set up input arguments

    #if TARGET is set add custom_target instead of ExternalProject_add
    set(Option TARGET EMPTY)  #if EMPTY set - generate empty target.
    #ProjectName , ProjectVersion , Variable to Add CMAKE_PREFIX_PATH , to add PATH , PKG_CONFIG_PATH
    #script add variable SOME_VAR_ORIG , and it must contains original value
    set(OneValueArgs PRJ ORIG_PRJ VER CMAKE PATH PKG TARGET_SUFFIX)
    #Variables to set and unset
    set(multiValueArgs SCOPED ENVS CACHED REMOVE)

    cmake_parse_arguments(  #Parse arguments
            PARAM # prefix of output variables
            "${Option}" # list of names of the boolean arguments (only defined ones will be true)
            "${OneValueArgs}" # list of names of mono-valued arguments
            "${multiValueArgs}" # list of names of multi-valued arguments (output variables are lists)
            ${ARGN} # arguments of the function to parse, here we take the all original ones
    )


    #Special case on gst-build - using original name GStreamer
    if ( PARAM_ORIG_PRJ )
        string(TOUPPER ${PARAM_ORIG_PRJ} PROJNAME )
        set( find_file Find${PARAM_ORIG_PRJ}.cmake )         #FindProject.cmake
    else ( PARAM_ORIG_PRJ )
        string(TOUPPER ${PARAM_PRJ} PROJNAME )
        set( find_file Find${PARAM_PRJ}.cmake )         #FindProject.cmake
    endif ( PARAM_ORIG_PRJ )

    if ( PARAM_TARGET_SUFFIX )
        set(TARGET_NAME ${PARAM_PRJ}${PARAM_TARGET_SUFFIX} )
    else ( PARAM_TARGET_SUFFIX )
        set(TARGET_NAME ${PARAM_PRJ}_config )
    endif ( PARAM_TARGET_SUFFIX )


    if ( PARAM_EMPTY )
        if ( PARAM_TARGET )

            add_custom_target(${TARGET_NAME} ALL
                    DEPENDS ${PARAM_PRJ}
                    COMMAND echo "  "
                    COMMENT ""
                    )

        else ( PARAM_TARGET )

            ExternalProject_Add_Step(${PARAM_PRJ} ${TARGET_NAME}
                    COMMAND echo "  "
                    COMMENT ""
                    DEPENDEES install
                    )

        endif ( PARAM_TARGET )

        return()
    endif ( PARAM_EMPTY )


    set( conf_file ${PROJNAME}-${PARAM_VER}.cmake ) #PROJECT-Version.cmake

    set( ROOT_DIR ${CMAKE_BINARY_DIR}/${PARAM_PRJ}  )  #Internal folder
    set( DEST_DIR ${CMAKE_INSTALL_PREFIX}/${PARAM_PRJ}/${PARAM_PRJ}-${PARAM_VER} ) #Destination folder
    set( PARAM_FILE ${ROOT_DIR}/${conf_file} )         #Full Path to PROJECT_Version.cmake file

    file(WRITE ${PARAM_FILE} "\n\n" )


    #############################################################################################
    # temporary version , unset all variables, which contains Project
    #############################################################################################

#    file(APPEND ${PARAM_FILE} "unset ( unset_vars )                                         \n" )
#    file(APPEND ${PARAM_FILE} "get_cmake_property(_variableNames VARIABLES)                 \n" )
#    file(APPEND ${PARAM_FILE} "foreach (_variableName \${_variableNames})                   \n" )
#    file(APPEND ${PARAM_FILE} "    string(FIND \${_variableName} \"${PARAM_PRJ}\" pos )     \n" )
#    file(APPEND ${PARAM_FILE} "    if (pos GREATER -1 AND pos LESS 2 ) #if pos = 0 or 1     \n" )
#    file(APPEND ${PARAM_FILE} "        list(APPEND unset_vars \${_variableName})            \n" )
#    file(APPEND ${PARAM_FILE} "    endif (pos GREATER -1 AND pos LESS 2 ) #if pos = 0 or 1  \n" )
#    file(APPEND ${PARAM_FILE} "endforeach()                                                 \n" )
#    file(APPEND ${PARAM_FILE} "                                                             \n" )
#    file(APPEND ${PARAM_FILE} "unset ( _variableNames )                                     \n" )
#    file(APPEND ${PARAM_FILE} "                                                             \n" )
#    file(APPEND ${PARAM_FILE} "foreach( var \${unset_vars})                                 \n" )
#    file(APPEND ${PARAM_FILE} "    message(STATUS \"unset variable : \${var}\")             \n" )
#    file(APPEND ${PARAM_FILE} "    unset( \${var} ) #scoped variable                        \n" )
#    file(APPEND ${PARAM_FILE} "    unset( \${var} CACHE) #cached variable                   \n" )
#    file(APPEND ${PARAM_FILE} "    unset( ENV{\${var}} ) #enviroment variable               \n" )
#    file(APPEND ${PARAM_FILE} "endforeach()                                                 \n" )
#    file(APPEND ${PARAM_FILE} "                                                             \n" )
#    file(APPEND ${PARAM_FILE} "unset ( unset_vars )                                         \n" )


    #############################################################################################
    # temporary version , unset all variables, which contains Project
    #############################################################################################

    if ( PARAM_REMOVE )

        file(APPEND ${PARAM_FILE} "\n\n" )
        file(APPEND ${PARAM_FILE} "#unset types for each extra variable\n" )

        #unset all variables in list REMOVE
        foreach( varName ${PARAM_REMOVE} )

            file(APPEND ${PARAM_FILE} "unset( ${varName} ) #scoped variable \n" )
            file(APPEND ${PARAM_FILE} "unset( ${varName} CACHE) #cached variable \n" )
            file(APPEND ${PARAM_FILE} "unset( ENV{${varName}} ) #enviroment variable \n" )

            file(APPEND ${PARAM_FILE} "\n" )
        endforeach()

    endif ( PARAM_REMOVE )


    file(APPEND ${PARAM_FILE} "#restore original CMAKE_PREFIX_PATH \n" )
    file(APPEND ${PARAM_FILE} "if( CMAKE_PREFIX_PATH_ORIG ) \n" )
    file(APPEND ${PARAM_FILE} "    set( CMAKE_PREFIX_PATH \${CMAKE_PREFIX_PATH_ORIG}) \n" )
    file(APPEND ${PARAM_FILE} "endif( CMAKE_PREFIX_PATH_ORIG ) \n\n" )


    file(APPEND ${PARAM_FILE} "#restore original ENV{PATH}\n" )
    file(APPEND ${PARAM_FILE} "if( PATH_ORIG ) \n" )
    file(APPEND ${PARAM_FILE} "    set( ENV{PATH} \${PATH_ORIG}) \n" )
    file(APPEND ${PARAM_FILE} "endif( PATH_ORIG ) \n\n" )


    file(APPEND ${PARAM_FILE} "#restore original ENV{PKG_CONFIG_PATH}\n" )
    file(APPEND ${PARAM_FILE} "if( PKG_CONFIG_PATH_ORIG ) \n" )
    file(APPEND ${PARAM_FILE} "    set( ENV{PKG_CONFIG_PATH} \${PKG_CONFIG_PATH_ORIG}) \n" )
    file(APPEND ${PARAM_FILE} "endif( PKG_CONFIG_PATH_ORIG ) \n\n" )


    #Get list of variables to set
    if ( PARAM_SCOPED )
        list(LENGTH PARAM_SCOPED scoped_count )
        math( EXPR scoped_count "${scoped_count} - 1")

        file(APPEND ${PARAM_FILE} "\n\n" )
        file(APPEND ${PARAM_FILE} "#set scoped variables\n" )

        foreach( pos RANGE 0 ${scoped_count} 2 )
            math( EXPR val_pos "${pos} + 1")

            list(GET PARAM_SCOPED ${pos} varName )
            list(GET PARAM_SCOPED ${val_pos} varValue )

            string(REPLACE ${CMAKE_INSTALL_PREFIX} "\${SMARTSDK_ROOT}" varValue ${varValue})

            file(APPEND ${PARAM_FILE} "set( ${varName} ${varValue}) \n" )

            message(STATUS "${PROJNAME}:${varName} = ${varValue}")
        endforeach()

    endif ( PARAM_SCOPED )

    if ( PARAM_CMAKE )  #define var , add it to #CMAKE_PREFIX_PATH
                        #and make backup of CMAKE_PREFIX_PATH as CMAKE_PREFIX_PATH_ORIG

        file(APPEND ${PARAM_FILE} "#append path to CMAKE_PREFIX_PATH\n" )
        file(APPEND ${PARAM_FILE} "set( CMAKE_PREFIX_PATH_ORIG \${CMAKE_PREFIX_PATH}) \n" )
        file(APPEND ${PARAM_FILE} "list(APPEND CMAKE_PREFIX_PATH ${PARAM_CMAKE}) \n\n" )

    endif( PARAM_CMAKE )

    if ( PARAM_PATH )  #define var , add it to #ENV{PATH}
                        #and make backup of ENV{PATH} as PATH_ORIG

        file(APPEND ${PARAM_FILE} "#append path to Enviroment variable PATH\n" )
        file(APPEND ${PARAM_FILE} "set( PATH_ORIG \$ENV{PATH}) \n" )
        file(APPEND ${PARAM_FILE} "set( ENV{PATH} ${PARAM_PATH}:\$ENV{PATH}) \n\n" )

    endif( PARAM_PATH )

    if ( PARAM_PKG )  #define var , add it to #ENV{PKG_CONFIG_PATH}
                        #and make backup of ENV{PKG_CONFIG_PATH} as PKG_CONFIG_PATH_ORIG

        file(APPEND ${PARAM_FILE} "#append path to Enviroment variable PKG_CONFIG_PATH \n" )
        file(APPEND ${PARAM_FILE} "set( PKG_CONFIG_PATH_ORIG \$ENV{PKG_CONFIG_PATH}) \n" )
        file(APPEND ${PARAM_FILE} "set( ENV{PKG_CONFIG_PATH} ${PARAM_PKG}:\$ENV{PKG_CONFIG_PATH}) \n\n" )

    endif( PARAM_PKG )


    if ( PARAM_ENVS )
        list(LENGTH PARAM_ENVS envs_count )
        math( EXPR envs_count "${envs_count} - 1")

        file(APPEND ${PARAM_FILE} "\n\n" )
        file(APPEND ${PARAM_FILE} "#set enviroment variables\n" )

        foreach( pos RANGE 0 ${envs_count} 2 )
            math( EXPR val_pos "${pos} + 1")

            list(GET PARAM_ENVS ${pos} varName )
            list(GET PARAM_ENVS ${val_pos} varValue )

            string(REPLACE ${CMAKE_INSTALL_PREFIX} "\${SMARTSDK_ROOT}" varValue ${varValue})

            file(APPEND ${PARAM_FILE} "set( ENV{${varName}} ${varValue}) \n" )

        endforeach()

    endif ( PARAM_ENVS )


    if ( PARAM_CACHED )
        list(LENGTH PARAM_CACHED cached_count )
        math( EXPR cached_count "${cached_count} - 1")

        file(APPEND ${PARAM_FILE} "\n\n" )
        file(APPEND ${PARAM_FILE} "#set cached variables\n" )

        foreach( pos RANGE 0 ${cached_count} 2 )
            math( EXPR val_pos "${pos} + 1")

            list(GET PARAM_CACHED ${pos} varName )
            list(GET PARAM_CACHED ${val_pos} varValue )

            string(REPLACE ${CMAKE_INSTALL_PREFIX} "\${SMARTSDK_ROOT}" varValue ${varValue})

            file(APPEND ${PARAM_FILE} "set( ${varName} ${varValue} CACHE STRING \"internal value\") \n" )

            message(STATUS "${PROJNAME}:${varName} = ${varValue}")
        endforeach()

    endif ( PARAM_CACHED )

    CONFIGURE_FILE( ${CMAKE_SOURCE_DIR}/cmake/TemplateFind.cmake
                    ${ROOT_DIR}/${find_file} COPYONLY  )         #Copy Template ( Find ) for project
    #Now add externalProject_step for copying
    #config and find files for project

    if ( PARAM_TARGET )

        add_custom_target(${TARGET_NAME} ALL
                DEPENDS ${PARAM_PRJ}
                COMMAND "${CMAKE_COMMAND}" -E copy_if_different ${PARAM_FILE}
                ${SMARTSDK_INST_PKG_DIR}/
                &&
                "${CMAKE_COMMAND}" -E copy_if_different ${ROOT_DIR}/${find_file}
                ${SMARTSDK_INST_FIND_WRAP_DIR}/
                COMMENT "copying cmake files to ${SMARTSDK_INST_PKG_DIR} and ${SMARTSDK_INST_FIND_WRAP_DIR}"
                )

    else ( PARAM_TARGET )

        ExternalProject_Add_Step(${PARAM_PRJ} ${TARGET_NAME}
                COMMAND "${CMAKE_COMMAND}" -E copy_if_different ${PARAM_FILE}
                ${SMARTSDK_INST_PKG_DIR}/
                &&
                "${CMAKE_COMMAND}" -E copy_if_different ${ROOT_DIR}/${find_file}
                ${SMARTSDK_INST_FIND_WRAP_DIR}/
                COMMENT "copying cmake files to ${SMARTSDK_INST_PKG_DIR} and ${SMARTSDK_INST_FIND_WRAP_DIR}"
                DEPENDEES install
                )

    endif ( PARAM_TARGET )

endfunction(GenerateConfigFile)