function(GeneratePkgConfig)

    #############################################################################################
    # get all neccessary info to fill PRJ.pc file and write it
    #############################################################################################


    #ProjectName , ProjectVersion , Variable to Add CMAKE_PREFIX_PATH , to add PATH , PKG_CONFIG_PATH
    #script add variable SOME_VAR_ORIG , and it must contains original value
    set(OneValueArgs PKG_FILE
                     PREFIX
                     EXEC_PREFIX
                     LIBDIR
                     INCLUDEDIR
                     NAME
                     DESC
                     VERSION
            )
    #Variables to set and unset
    set(multiValueArgs REQUIRES
                       CONFLICTS
                       LIBS_DIRS
                       LIBS_NAMES
                       CFLAGS
            )

    cmake_parse_arguments(  #Parse arguments
            PARAM # prefix of output variables
            "" # list of names of the boolean arguments (only defined ones will be true)
            "${OneValueArgs}" # list of names of mono-valued arguments
            "${multiValueArgs}" # list of names of multi-valued arguments (output variables are lists)
            ${ARGN} # arguments of the function to parse, here we take the all original ones
            )

    #############################################################################################
    # convert each cmake list to VAR1\ VAR2\ etc.
    #############################################################################################


    foreach(require ${PARAM_REQUIRES})
        set(REQUIRES ${require}\ ${REQUIRES})
    endforeach()
    foreach(conflict ${PARAM_CONFLICTS})
        set(CONFLICTS ${conflict}\ ${CONFLICTS})
    endforeach()
    foreach(libs_dir ${PARAM_LIBS_DIRS})
        set(LIBS_DIRS -L${libs_dir}\ ${LIBS_DIRS})
    endforeach()
    foreach(libs_name ${PARAM_LIBS_NAMES})
        set(LIBS_NAMES -l${libs_name}\ ${LIBS_NAMES})
    endforeach()
    foreach(cflag ${PARAM_CFLAGS})
        set(CFLAGS -I${cflag}\ ${CFLAGS})
    endforeach()

    set(LFLAGS ${LIBS_DIRS}\ ${LIBS_NAMES})

    #############################################################################################
    # write file part
    #############################################################################################

    file(WRITE  ${PARAM_PKG_FILE} "prefix=${PARAM_PREFIX}               \n" )

    if (PARAM_EXEC_PREFIX)
        file(APPEND ${PARAM_PKG_FILE} "exec_prefix=${PARAM_EXEC_PREFIX} \n" )
    else(PARAM_EXEC_PREFIX)
        file(APPEND ${PARAM_PKG_FILE} "exec_prefix=\${prefix}           \n" )
    endif(PARAM_EXEC_PREFIX)

    if (PARAM_LIBDIR)
        file(APPEND ${PARAM_PKG_FILE} "libdir=${PARAM_LIBDIR}           \n" )
    else(PARAM_LIBDIR)
        file(APPEND ${PARAM_PKG_FILE} "libdir=\${prefix}/lib            \n" )
    endif(PARAM_LIBDIR)

    if (PARAM_INCLUDEDIR)
        file(APPEND ${PARAM_PKG_FILE} "includedir=${INCLUDEDIR}         \n" )
    else(PARAM_INCLUDEDIR)
        file(APPEND ${PARAM_PKG_FILE} "includedir=\${prefix}/include    \n" )
    endif(PARAM_INCLUDEDIR)

    file(APPEND ${PARAM_PKG_FILE} "\n\n" )

    file(APPEND ${PARAM_PKG_FILE} "Name: ${PARAM_NAME}                  \n" )
    file(APPEND ${PARAM_PKG_FILE} "Description: ${PARAM_DESC}           \n" )
    file(APPEND ${PARAM_PKG_FILE} "Version: ${PARAM_VERSION}            \n" )
    file(APPEND ${PARAM_PKG_FILE} "Requires: ${REQUIRES}                \n" )
    file(APPEND ${PARAM_PKG_FILE} "Conflicts: ${CONFLICTS}              \n" )
    file(APPEND ${PARAM_PKG_FILE} "Libs: ${LFLAGS}                      \n" )
    file(APPEND ${PARAM_PKG_FILE} "Cflags: ${CFLAGS}                    \n" )

    #############################################################################################
    # write part end
    #############################################################################################


endfunction(GeneratePkgConfig)